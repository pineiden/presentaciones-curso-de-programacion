import csv
from itertools import groupby

def load_data(file_path):
    """
    Lee csv, cada fila será un dict
    Revisa si el dato en csv existe, entonces lo reemplaza en json
    """
    fields = ["posicion", "fecha", "titulo", "resumen"]
    posiciones = {1:0, 3:1} 
    sesiones = []
    with open(file_path) as programa:
        reader = csv.DictReader(programa, delimiter=';')
        for row in reader:
            row["posicion"]= int(row["posicion"])
            filtered = {
                key: value
                for key, value in row.items() if key in fields
            }
            sesiones.insert(filtered.get("posicion"),
                                               filtered)

    sesiones_dict = {}
    data = sorted(sesiones, key=lambda e:e.get('posicion'))
    for k, g in groupby(data, lambda e: e.get('posicion')):
        if k not in sesiones_dict.keys():
            sesiones_dict[k] = []
        sesiones_dict[k].append(*list(g))
    return sesiones_dict


if __name__ == "__main__":
    print(load_data('./programa.csv'))
