* Ejercicio 20

Este ejercicio continua con la visualización de la frecuencia de palabras en los
cuentos seleccionados.  
 
La solución puede resultar para diferentes conjuntos de textos.

Veremos el uso de diferentes soluciones. Una que podría ser una implementación
sencilla y otra que aprovecha las características de la estructura de datos
'array hash' que dispone *awk*.

Ahora, el script básico es el siguiente, Presenta el problema de que la búsqueda
por coincidencias no resulta ser tan precisa.

#+BEGIN_SRC bash
read -p "Que palabras buscas " PALABRAS
palabras=$(echo $PALABRAS|tr [:upper:] [:lower:])
awk -F',' -v palabras="$palabras" '(NR==1){
         print}(match(palabras,$1)){print}' tabla_frecuencias.csv
#+END_SRC

La solución base consiste en lo siguiente.

- pasamos la lista de palabras a un array
- creamos funcion que itera la palabra a buscar sobre el array.


* Solución iterando sobre la lista de palabras

#+BEGIN_SRC awk
  function pertenencia(palabra, array){
      for (indice in array){
          if (palabra == array[indice]){
              return 1
          }
      }
      return 0
  }

  BEGIN{
      split(palabras, array_palabras, " ")
  }
  (NR==1){
      print
  }
  {
      palabra=$1;
      if (pertenencia(palabra,array_palabras)==1){
          print
      }
  }
#+END_SRC


Ejecutamos el script

#+BEGIN_SRC bash
awk -v palabras="árboles nube" -F',' -f filtrar.awk tabla_frecuencias.csv
#+END_SRC


Para crear el perfile

#+BEGIN_SRC bash
awk --profile="iteracion.prof" -v palabras="árboles nube" -F',' -f filtrar.awk tabla_frecuencias.csv
#+END_SRC


El perfil resultante

#+BEGIN_EXAMPLE
	# perfil de gawk, creado Tue Dec 22 23:13:13 2020

	# BEGIN regla(s)

	BEGIN {
     1  	FS = ","
     1  	OFS = ","
     1  	split(palabras, array_palabras, " ")
	}

	# Regla(s)

  4107  (NR == 1) { # 1
     1  	print
	}

  4107  {
  4107  	palabra = $1
  4107  	if (pertenencia(palabra, array_palabras) == 1) { # 2
     2  		print
		}
	}


	# Funciones, enumeradas alfabéticamente

  4107  function pertenencia(palabra, array)
	{
  8213  	for (indice in array) {
  8213  		if (palabra == array[indice]) { # 2
     2  			return 1
			}
		}
  4105  	return 0
	}
#+END_EXAMPLE

Si vemos el tiempo que demora en operar todo el trabajo:

#+BEGIN_EXAMPLE
real	0m0.009s
user	0m0.009s
sys	0m0.000s
#+END_EXAMPLE

  
* Solución mediante referencia a posición

En este caso invertimos el array, en que cada palabra será una llave del arreglo
y, por lo tanto, será de uso directo sin necesidad de iterar para buscar coincidencia.  

Se crea una función que invierta y otra que revise la pertenencia.

#+BEGIN_SRC awk
function reverse(array, reverse_array){
    for (elem in array){
        palabra=array[elem]
        reverse_array[palabra]=1
    }
}

function pertenencia(palabra, array){
    resultado=array[palabra]==1?array[palabra]:0
    return resultado
}

BEGIN{
    split(palabras, array_palabras, " ");
    reverse(array_palabras, reverse_palabras)
}
(NR==1){
    print
}
{
    palabra=$1;
    if (pertenencia(palabra,reverse_palabras)==1){
        print
    }
}
#+END_SRC

#+BEGIN_SRC bash
awk -v palabras="árboles nube" -F',' -f filtrar.awk tabla_frecuencias.csv
#+END_SRC

Ejecutamos el script de la misma manera

Para crear el perfile

#+BEGIN_SRC bash
awk --profile="dict_reverse.prof" -v palabras="árboles nube" -F',' -f filtrar.awk tabla_frecuencias.csv
#+END_SRC

#+BEGIN_EXAMPLE
	# perfil de gawk, creado Tue Dec 29 09:52:32 2020

	# BEGIN regla(s)

	BEGIN {
     1  	FS = ","
     1  	OFS = ","
     1  	split(palabras, array_palabras, " ")
     1  	reverse(array_palabras, reverse_palabras)
	}

	# Regla(s)

  4107  (NR == 1) { # 1
     1  	print
	}

  4107  {
  4107  	palabra = $1
  4107  	if (pertenencia(palabra, reverse_palabras) == 1) { # 2
     2  		print
		}
	}


	# Funciones, enumeradas alfabéticamente

  4107  function pertenencia(palabra, array)
	{
  4107  	resultado = array[palabra] == 1 ? array[palabra] : 0
  4107  	return resultado
	}

     1  function reverse(array, reverse_array)
	{
     2  	for (elem in array) {
     2  		palabra = array[elem]
     2  		reverse_array[palabra] = 1
		}
	}

#+END_EXAMPLE

Si bien, este es un caso pequeño. El ahorranos en dos líneas el doble de la
evaluación nos permitirá ser más rápidos y eficientes. El algoritmo demorará
menos y consumirá menos energía

#+BEGIN_EXAMPLE
real	0m0.008s
user	0m0.008s
sys	0m0.000s
#+END_EXAMPLE


* Solución verificando pertenencia

En este caso invertimos el array, en que cada palabra será una llave del arreglo
y, por lo tanto, será de uso directo sin necesidad de iterar para buscar
coincidencia.   Tal como en el segundo caso. 

Ahora, haremos uso de las características de *awk* que nos permite verificar
directamente si un elemento (índice o llave) pertenece al array, usando *key in array*.

#+BEGIN_SRC awk
function reverse(array,reverse_array){
    for (elem in array){
        palabra=array[elem]
        reverse_array[palabra]=1
    }
}

BEGIN{
    split(palabras, array_palabras, " ");
    reverse(array_palabras, reverse_palabras)
}
(NR==1){
    print
}
{
    palabra=$1;
    if (palabra in reverse_palabras){
        print
    }
}
#+END_SRC

#+BEGIN_SRC bash
awk -v palabras="árboles nube" -F',' -f filtrar_optimo_in.awk tabla_frecuencias.csv
#+END_SRC

Ejecutamos el script de la misma manera

Para crear el perfile

#+BEGIN_SRC bash
awk --profile="perfil3.prof" -v palabras="árboles nube" -F',' -f filtrar_optimo_in.awk tabla_frecuencias.csv
#+END_SRC

Reultando un perfil similar al segundo. Salvo de que tiene la perticularidad de
que no ejecuta la función /pertenencia/ 4107 veces. Esto para un número mayor
podría ser influyente en términos de eficiencia.

#+BEGIN_EXAMPLE
	# perfil de gawk, creado Tue Dec 29 09:49:19 2020

	# BEGIN regla(s)

	BEGIN {
     1  	FS = ","
     1  	OFS = ","
     1  	split(palabras, array_palabras, " ")
     1  	reverse(array_palabras, reverse_palabras)
	}

	# Regla(s)

  4107  (NR == 1) { # 1
     1  	print
	}

  4107  {
  4107  	palabra = $1
  4107  	if (palabra in reverse_palabras) { # 2
     2  		print
		}
	}


	# Funciones, enumeradas alfabéticamente

	function pertenencia(palabra, array)
	{
		resultado = array[palabra] == 1 ? array[palabra] : 0
		return resultado
	}

     1  function reverse(array, reverse_array)
	{
     2  	for (elem in array) {
     2  		palabra = array[elem]
     2  		reverse_array[palabra] = 1
		}
	}
#+END_EXAMPLE

El tiempo de ejecución resulta en.

#+BEGIN_EXAMPLE
real	0m0.008s
user	0m0.004s
sys	0m0.004s
#+END_SRC

* Crear gráfica multibarra con gnuplot 
  
Se crea una gráfica de barras, con estilo de histogramas, tomando del *stream*.

Nuestro script inicial se convierte a lo siguiente, utilizando la solución
óptima (la tercera).

#+BEGIN_SRC bash
read -p "Que palabras buscas " PALABRAS
palabras=$(echo $PALABRAS|tr [:upper:] [:lower:])
awk -v palabras="$palabras" -f filtrar_opt_in.awk tabla_frecuencias.csv
#+END_SRC

Código gnuplot

#+BEGIN_SRC gnuplot
set xlabel "Palabras"
set ylabel "Cantidad"
set datafile separator ","
set title "Cantidad de Palabras en Cuentos"
set xtics rotate by -45
set style fill solid 0.5
set boxwidth 0.1 relative
plot "< cat -" using 2:xtic(1) with histogram title "Amorosa" lt rgb "#406090", \
    "" using 3 with  histogram title "¡Adiós" lt rgb "#402200"
    "" using 4 with  histogram title "Aparición" lt rgb "#10AA00"
    "" using 5 with  histogram title "Abandonado" lt rgb "#0044AA"
    "" using 6 with  histogram title "Antón" lt rgb "#00CC33"
    "" using 7 with  histogram title "Ahogado" lt rgb "#33CC00"
    "" using 8 with  histogram title "A caballo" lt rgb "#CC0333"
    "" using 8 with  histogram title "Amor" lt rgb "#EEAA00"
#+END_SRC

Terminaría resultando en:

#+BEGIN_SRC bash
read -p "Que palabras buscas " PALABRAS
palabras=$(echo $PALABRAS|tr [:upper:] [:lower:])
awk -v palabras="$palabras" -f filtrar_opt.awk tabla_frecuencias.csv > seleccion.csv
gnuplot graficar.gp
#+END_SRC

Para la búsqueda:
"árboles nube déjame"

Tendremos.

[[file:./prueba_busqueda.png]]
