@include "pertenencia_opt.awk"

BEGIN{
    FS=",";
    OFS=",";
    split(palabras, array_palabras, " ");
    reverse(array_palabras, reverse_palabras)
}
(NR==1){
    print
}
{
    palabra=$1;
    if (palabra in reverse_palabras){
        print
    }
}
