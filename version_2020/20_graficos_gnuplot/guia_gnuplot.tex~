% Created 2020-12-21 lun 15:51
% Intended LaTeX compiler: lualatex
\documentclass[11pt]{article}
\usepackage{graphicx}
\usepackage{grffile}
\usepackage{longtable}
\usepackage{wrapfig}
\usepackage{rotating}
\usepackage[normalem]{ulem}
\usepackage{amsmath}
\usepackage{textcomp}
\usepackage{amssymb}
\usepackage{capt-of}
\usepackage{hyperref}
\usepackage[spanish]{babel}
\usepackage{setspace}
\usepackage{fontspec}
\onehalfspacing
\author{Camilo Carrasco Díaz}
\date{\today}
\title{Fuentes de informacion y como descargarla}
\hypersetup{
 pdfauthor={Camilo Carrasco Díaz},
 pdftitle={Fuentes de informacion y como descargarla},
 pdfkeywords={},
 pdfsubject={},
 pdfcreator={Emacs 26.1 (Org mode 9.4.3)}, 
 pdflang={Spanish}}
\begin{document}

\maketitle
\setcounter{tocdepth}{1}
\tableofcontents


\section{Temario de la clase}
\label{sec:org72ab0ae}

\subsection{¿Qué veremos en esta clase?}
\label{sec:orgd0feca8}

\begin{description}
\item[{Introducción a visualizar datos}] los elementos conceptuales necesarios
para comprender y crear visualizaciones de conjuntos de datos de manera
eficaz.

\item[{Concepto de escala}] cuando es necesario representar datos se recurre a
herramientas de representación, el concepto de \emph{escala} es clave.

\item[{Series de datos}] como una tabla de datos se puede representar en una
figura.

\item[{Dimensión}] el concepto de dimensiones y sus posiblidades.

\item[{Creación de funciones}] crear funciones y almacenar sus resultados en
tablas.

\item[{Ubicación en el plano cartesiano}] en qué consiste el plano cartesiano y
como se puede ubicar un punto en referencia a su centro.

\item[{Coordenadas cartesianas y polares}] comparación y demostración de los
tipos de coordenadas diferentes.

\item[{Uso de GNUPlot}] graficar series de datos desde la terminal.

\item[{Las partes importantes para un gráfico}] Cómo identificar y definir
distintas características de las partes importantes de un gráfico.

\item[{Gráficas conocidas}] serie tiempo, histograma, barras, scatter, etc.
\end{description}

\section{Registrar inicio de clases}
\label{sec:org49f4d84}

  Para poder realizar una búsqueda que tenga un mayor contexto y precisión, 
es posible añadir más texto a la seña que nos permita mejorar la referencia.

Recomiendo añadir, al número de la clase, la seña relacionada al nombre de la
clase, que en este caso sería \textbf{gnuplot\_01}.

\begin{verbatim}
echo "CLASE22::gnuplot_01"
\end{verbatim}

Anota en tu cuaderno la fecha y la seña \textbf{CLASE\_22::gnuplot\_01}

\section{Instalar software de visualizaicón}
\label{sec:org14b46ef}

\subsection{GnuPlot}
\label{sec:org6535f13}

sudo apt install gnuplot

\subsection{Graphviz}
\label{sec:org04ab082}

sudo apt install graphviz

\section{Visualización de datos}
\label{sec:org6a7b878}

Este documento es una introducción general a la \textbf{visualización de datos}. Si
bien se aplica con las herramientas de la línea de comandos \textbf{gnuplot} y
\textbf{graphviz}, es aplicable a cuaquier sistema de creación de gráficas.

Antes de llegar y ejecutar comandos \emph{sin comprender} lo que se hace,
realizaremos un estudio al detalle de cómo se puede crear una gráfica y sus
componentes principales.

De cada sección se realizarán ejercicios \emph{análogos}, que nos permitan llegar a
un conocimiento acabado de los detalles de este conocimiento en particular.

El objetivo de este capítulo es lograr una base de conocimiento general para
que el creador o la creadora de una gráfica se atreva a innovar y mejorar la
visualización hasta el punto de sentirse satisfech@ de lo que quiere representar.

\subsection{Anatomía de un gráfico}
\label{sec:org441729c}

Cuando hablamos de un \textbf{gráfico} o \textbf{visualización} estamos refiriendonos a una
\emph{forma de representar} una serie de datos que están relacionados de manera
coherente y, en general, ordenados.

La siguiente figura es \textbf{muy importante}, ya que te servirá como referencial para
conocer las partes que componen un gráfico común y te dará a entender los
\emph{diferentes sistemas} de creación de gráficos.

En general, cada lenguaje de uso común, como \textbf{python}, \textbf{javacsript}, \textbf{c++},
etcétera. Tienen sus propias bibliotecas para creación de gráficas. Más allá de
las diferencias, los principios son generales.

\begin{center}
\includegraphics[width=.9\linewidth]{./img/anatomia_grafico.svg.png}
\end{center}

Con esto, será posible identificar los siguientes elementos:

\begin{itemize}
\item elementos de texto
\item elementos de información como leyendas
\item elementos de medida
\item líneas gráficas
\item grillas
\item puntos en scatter
\item etcétera
\end{itemize}

\subsection{Relación entre datos y un gráfico}
\label{sec:org1418939}

Los gráficos no aparecen por generación espontánea. Hay que tener claros que
estos se construyen para \textbf{representar} lo que son \textbf{set de datos}.

Estos datos pueden estar en \textbf{archivos}, \textbf{bases de datos}, distribuidos a través
de directorios. Contienen distintos tipos de información, sean mediciones de
distintos tipos de fenómenos, abtracciones, o ¡invenciones también!

En general núnca va a ser directo tomar los datos y gráficar. Será necesario

\begin{description}
\item[{ordenar}] aplicar criterios de ordenamiento
\item[{seleccionar}] definir cualitativamente que datos se filtrarán
\item[{filtrar}] definir cuantitativamente los datos a graficar
\item[{formatear}] ajustar el dato a una forma estándar
\end{description}

Al menos, estos datos, deben pasar por estas cuatro acciones \emph{antes de
graficar}. Los datos erroneos dan a gráficos erróneos. Por lo que es importante
validar cada dato antes de ser visualizado. En este sentido se habla no de
eliminar datos de un \textbf{dataset} que sean extraños. Sino hablamos de \textbf{tipo de
dato}.

Cuando se habla de un \textbf{tipo de dato}, es que se habla de que las características
de cada dato se pueden agrupar con el resto en común. Es decir, todos los datos
deben ser del \textbf{mismo tipo de dato}, para que sea posible \emph{comparar}.

Supongamos que tenemos una serie de datos bien formateada, y todo el trabajo
pesado está listo. Esta serie de datos la genera una función trigonométrica
\textbf{seno}.

Entramos a la \textbf{shell} de \textbf{gnuplot}

\begin{verbatim}
gnuplot
\end{verbatim}


Con este comando creamos la gráfica para la función trigonométrica \textbf{seno}.

\begin{verbatim}
plot sin(x)
\end{verbatim}

El comando nos entregará la representación gráfica de la función.

\begin{center}
\includegraphics[width=.9\linewidth]{./img/sin.png}
\end{center}

\subsection{La fuerza de una buena visualización.}
\label{sec:orgb8f604a}

La gráfica anterior, más allá de su simplicidad, no nos dice nada. Es bueno
entregar más información que entregue contexto a la imagen.

Añadimos al código (en la shell de gnuplot) las características visuales

\begin{itemize}
\item entregar título general
\item entregar título por eje
\item mostrar leyenda
\item activar grilla
\end{itemize}

Definimos título y etiquetas de ejes.

\begin{verbatim}
set encoding utf-8
set title "Sin(x)"
set xlabel "Tiempo [ms]"
set ylabel "Amplitud [m]"
set yrange [-1:1]
plot 5*sin(x)
\end{verbatim}

Es muy parecido, salvo que ahora nos entrega información respecto a qué cosa está graficando.

Ahora procederemos a agregar algunos estilos para que sea más distinguible la información.

\begin{verbatim}
set title "Sin(x)"
set xlabel "Tiempo [ms]" textcolor "blue"
set ylabel "Amplitud [m]" textcolor "red"
set key top left box opaque
set linestyle 1 \
  linecolor rgb "#FF7701" \
  linetype 1 linewidth 2 \
  pointtype 4 pointsize 1.5 
plot sin(x) with linespoint linestyle 1
\end{verbatim}

Si observamos, cada línea otorga ciertas características a algún elemento de la gŕafica.

\begin{description}
\item[{la leyenda}] se ubica a la izquierda arriba en una caja
\item[{la línea}] pasa a ser línea un puntos, naranja
\item[{leyenda x}] en azul
\item[{leyenda y}] en rojo
\end{description}


\section{La idea de escala y offset}
\label{sec:org2ceb8d9}

Cuando nos encomendamos en la tarea de graficar una serie de datos o funciones
específicas debemos ser concientes que lo que visualiza es una \textbf{representación}
y no es exactamente del mismo tamaño o lo mismo a lo que deseamos representar.

Así como en las obras de artes visuales podemos encontrarnos con muros
gigantescos que representan humanos o en los mapas nos podemos encontrar con la
descripción de un territorio y sus características más importantes, al momento
de visualizar debemos ser conciente de que existe una \textbf{escala} que tendrá
relación entre la realidad y lo representado.

\subsection{Offset}
\label{sec:org7fbc468}

En una pantalla cada elemento se posiciciona según un motor gráfico que
determina la ubicación dentro de ella.

Podemos decir que hay dos elementos básicos:

\begin{description}
\item[{superficie}] donde se ubican todos los elementos gráficos
\item[{lienzo}] donde se posiciona en particular una gráfica
\end{description}

Cuando se dibuja un elemento sobre el \textbf{lienzo} se hace en base al punto 0 de
referencia de este. Ahora bien, el \textbf{lienzo} se ubica en cierta posición respecto
la \textbf{superficie}, por lo que cualquier elemento dentro del \textbf{lienzo} estará
posicionado respecto a la \textbf{superficie} en relación a su posición con el \textbf{lienzo}
y un \textbf{offset}.

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./img/offset.png}
\caption{Posicionamiento en offset de un elemento gráfico}
\end{figure}

\subsection{Escala}
\label{sec:org6868ffe}

La \textbf{escala} es una relación o \textbf{función} que determina el punto de llegada de un
dato a un \textbf{lienzo} dónde se realizará un dibujo o gráfico. La posición final de
cada elemento estará en relación al offset y lo que resulte de la \textbf{escala}.

Si se tiene un paisaje con un árbol de 15m de altura, y deseamos dibujarlo en un
block de 30x20 cm2.

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./img/escala.png}
\caption{Árbol de 15m, ¿Cómo cabe aquí?}
\end{figure}

Deberíamos primero pensar en normalizar la escala. Usaremos unidades de sistema
internacional.

\begin{center}
\begin{tabular}{lr}
Elemento & Medida\\
\hline
Árbol & 15\\
Block & .2\\
\end{tabular}
\end{center}

La \textbf{escala} para poder dibujar tal árbol a \emph{escala} sería calcular

\begin{verbatim}
BEGIN{print 15/.2}
\end{verbatim}

Será que tendremos que reducir 75 veces cada elemento.

Es decir la \textbf{escala será *75:1}. Por cada centímetro que queramos dibujar de la
realidad, deberemos \emph{dividirlo} en 75.

\subsection{Crear funciones de escala}
\label{sec:org326cf0f}

Dada la serie de datos siguiente, de posición y velocidad de un vehículo a lo
largo de un viaje de \textbf{Santiago} a \textbf{Puerto Montt}.

\begin{table}[htbp]
\label{viaje_stgo_pto_montt}
\centering
\begin{tabular}{rr}
Ubicación [km] & Velocidad [km/hr]\\
\hline
0 & 110\\
120 & 140\\
200 & 100\\
350 & 90\\
480 & 50\\
560 & 120\\
660 & 120\\
790 & 130\\
870 & 100\\
950 & 130\\
1032 & 90\\
\end{tabular}
\end{table}

¿Cuál sería la escala para graficar la relación en un gráfico de de 15cm de ancho
y 8cm de alto?

¿Cuál fue la velocidad promedio y su variabilidad?

¿Cuál sería el código para graficar estos datos?

\begin{verbatim}
set encoding utf-8
set key box
set xlabel "Ubicacion [km]"
set ylabel "Velocidad [km/hr]"
set size ratio .6
set xtics 0,100,1000
set ytics 40,10,150
plot velocidad
\end{verbatim}

\begin{center}
\includegraphics[width=.9\linewidth]{./ejemplos/viaje.png}
\end{center}


Ahora bien, realizaremosalgunas ajustes estéticos

\begin{verbatim}
set encoding utf8
set key box
set xlabel "Ubicacion [km]" textcolor "black"
set ylabel "Velocidad [km/hr]" textcolor "black"
set size ratio .6
set xtics 0,100,1000
set ytics 40,10,150
set linestyle 1 \
  linecolor rgb "#00AA55" \
  linetype 2 linewidth 2 \
  pointtype 6 pointsize 1.5 
plot velocidad  with linespoint linestyle 1 
\end{verbatim}

\begin{center}
\includegraphics[width=.9\linewidth]{./ejemplos/viaje2.png}
\end{center}


Ahora, si deseamos suavizar la línea podemos setear una \textbf{spline} que es un tipo de curva que permite suavizar gráficas.

\begin{verbatim}
set encoding utf8
set key box
set xlabel "Ubicacion [km]" textcolor "black"
set ylabel "Velocidad [km/hr]" textcolor "black"
set size ratio .6
set xtics 0,100,1000
set ytics 40,10,150
set linestyle 1 \
  linecolor rgb "#CC3355" \
  linetype 2 linewidth 2 \
  pointtype 6 pointsize 1.5 
plot velocidad  smooth csplines with linespoint linestyle 1 
\end{verbatim}

Resulta una gráfica muy similar pero suavizada. 

\begin{center}
\includegraphics[width=.9\linewidth]{./ejemplos/viaje3.png}
\end{center}


\section{Series de datos}
\label{sec:org2cb83db}

Se desea contar la cantidad de palabras que contiene cada cuento en el directorio \textbf{ejemplos/cuentos}

\begin{verbatim}
ls ./ejemplos/cuentos
\end{verbatim}

Los cuentos son de la colección \textbf{Cuentos completos de Guy de Mauppasant}.

Creamos un script de \textbf{awk} que permita contar las palabras que existen en cada texto.

Descartamos puntuaciones y dígitos. Pasamos series de espacios en blanco a un
solo espacio. Cada palabra la pasamos a minúsculas y acumulamos con estas el
conteo. 

Usamos el bloque \textbf{ENDFILE} para setear el título al terminar de leer cada
archivo. Este se obtiene al iniciar la primera línea de cada archivo usando FNR.

Luego, debemos considerar las palabras conectoras y descargarlas del conteo. El
archivo es \textbf{descarte.list}. 

\begin{verbatim}
function descartar(word, array_descarte){
    for (elem in array_descarte){
        if (word==array_descarte[elem]){
            return 1
        }
    }
    return 0
}
\end{verbatim}

Luego incluimos la biblioteca y hacemos el conteo de palabras.

\begin{verbatim}
 #! /bin/awk -f
@include "palabras.awk"
BEGIN{
OFS="|";
print "title","word","amount of words"
}
{FNR==NR}{
 array_descarte[NR]=$0
} 
(FNR==1){
titulo=$0;
}
{
gsub(/[[:punct:][:digit:]]/,"",$0);
gsub(/[[:blank:]]{2}/," ",$0);
for (pos=1;pos<=NF;pos++){
word=tolower($pos);
words[FILENAME][word]++
total++
}
}
ENDFILE{
titles[FILENAME]=titulo
}
END{
for (file in words){
    for (word in words[file]){
      if (descartar(word,array_descarte)==0){
          print titles[file], word, words[file][word]
      }
    }
}
total=0;
}
\end{verbatim}

Luego, ejecutamos el script para todos los archivos. Damos permisos de ejecución al script y luego lo corremos.

\begin{verbatim}
chmod a+x contar_palabras.awk
./contar_palabras.awk ejemplos/cuentos/* > conteo_palabras.psv
\end{verbatim}

Usamos la extensión \textbf{psv} ya que samos la pleca \emph{|} como elemento separados.

Luego, con esto podemos guardar una selección.

\begin{verbatim}
awk -f descarte.awk conteo_palabras.psv > seleccion.psv
\end{verbatim}

\subsection{Graficar la cantidad de palabras en cada cuento.}
\label{sec:org8426df8}

Dada una selección de palabras, graficar en barras la frecuencia que tenga en
cada cuento.

\begin{enumerate}
\item ¿Cómo hacer un gráfico de barras?
\label{sec:org44cf147}

Los gráficos de barra sirven para mostrar visualizaciones de conjuntos de
datos discretos, como palabras o días o meses, Permiten mostrar cantidades
porcentajes o fracciones.

\begin{table}[htbp]
\label{temp}
\centering
\begin{tabular}{rlr}
Id & Día & Temperatura [°C]\\
\hline
1 & lunes & 31\\
2 & martes & 28\\
3 & miércoles & 33\\
4 & jueves & 23\\
5 & viernes & 28\\
6 & sábado & 22\\
7 & domingo & 33\\
\end{tabular}
\end{table}

Escribimos el código para gráficar en barras

\begin{verbatim}
set xlabel "Día"
set ylabel "Temperatura [°C]"
set datafile separator ","
set yrange [0:50]
set title "Temperaturas de la semana"
set xtics rotate
set style fill solid 0.5
set boxwidth 0.5 relative
set term png
set output "temperaturas.png"
plot "temperaturas.csv" using 0:3:xticlabels(2) with boxes title "temperatura"
\end{verbatim}

La selección de columnas se interpreta a continuación:

\begin{description}
\item[{0}] le dice a gnuplot ubicar las barras en el mismo orden del archivo
\item[{3}] tomar la columna 3
\item[{xticlabels(2)}] toma los nombres de tics en eje x de la columna 2.
\end{description}

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./ejemplos/temperaturas.png}
\caption{Gráfica de temperaturas de una semana}
\end{figure}


\item ¿Cómo hacer un gráfico compuesto?
\label{sec:org49c2bb1}

Si se tienen las temperaturas de dos ciudades: Santiago y Puerto Montt

\begin{itemize}
\item Graficar en barras las temperaturas de cada ciudad
\item Graficar las diferencias entre Santiago y Puerto Montt de cada día.
\end{itemize}


\begin{table}[htbp]
\label{temp}
\centering
\begin{tabular}{rlrr}
Id & Día & Temp Santiago [°C] & Temp Pto. Montt [°C]\\
\hline
1 & lunes & 31 & 21\\
2 & martes & 28 & 17\\
3 & miércoles & 33 & 16\\
4 & jueves & 23 & 18\\
5 & viernes & 28 & 22\\
6 & sábado & 22 & 23\\
7 & domingo & 33 & 27\\
\end{tabular}
\end{table}


Se utiliza el estilo definido por \textbf{histograms} para la visualización de
histogramas y gráficas de barras.

\begin{verbatim}
set xlabel "Día"
set ylabel "Temperatura [°C]"
set datafile separator ","
set style data histograms

set yrange [0:40]
set xrange [0:8]
set title "Comparación de temperaturas Stgo vs Pto Montt"
set xtics rotate by -45
set style fill solid 0.5
set boxwidth 0.5 relative
set term png
set output "temp_ciudades.png"
plot "temps_ciudades.csv" using 3 with histogram title "Temp. Santiago" lt rgb "#406090", \
     "" using 4:xtic(2) with histogram title "Temp. Pto Montt" lt rgb "#40FF00"
\end{verbatim}

Resulta en la gráfica

\begin{figure}[htbp]
\centering
\includegraphics[width=.9\linewidth]{./ejemplos/temp_ciudades.png}
\caption{Temperaturas de dos ciudades.}
\end{figure}

\item Tomar datos de un archivo o stream
\label{sec:org7f14e08}

Podemos también tomar desde un stream los datos, por ejemplo calculando la
diferencia de temperatura.    

Para obtener el valor de la diferencia.

\begin{verbatim}
awk -F',' 'BEGIN{
    print "id","día","diferencia";
    OFS=","}
    (NR>1){print $1,$2,int($3-$4)
    }' temps_ciudades.csv |gnuplot --persist from_stream.gp
\end{verbatim}

Este resultado lo encadenamos a \textbf{gnuplot}

\begin{verbatim}
set xlabel "Día"
set ylabel "Diferencia de Temperatura [°C]"
set datafile separator ","
set yrange [-10:20]
set xrange [0:8]
set title "Comparación de temperaturas Stgo vs Pto Montt"
set xtics rotate by -45
set style fill solid 0.5
plot "< cat -" using 0:3:xtic(2) with lines title "Diferencias de Temperatura"
\end{verbatim}

Para tomar datos de la salida estándar se utiliza, en vez de un nombre de
archivo, \textbf{< cat -}. 

\item Hacer un multiplot
\label{sec:org68c525d}

Podemos graficar en dos recuadros separados las temperaturas de cada ciudad y
las diferencias.

Al momento de gráficar las diferencias, se puede calcular expresivamente como:

\begin{verbatim}
i = 8
j = 2
plot "test.dat" using 1:(column(i)-column(j)) w lp
\end{verbatim}

Se define un multiplot de \textbf{2 filas y 1 columna}

\begin{verbatim}
set multiplot layout 2,1 rowsfirst
set grid
set xlabel "Día"
set ylabel "Temperatura [°C]"
set datafile separator ","
set style data histograms

set yrange [0:40]
set xrange [0:8]
set title "Comparación de temperaturas Stgo vs Pto Montt"
set xtics rotate by -45
set style fill solid 0.5
set boxwidth 0.5 relative
plot "temps_ciudades.csv" using 3 with histogram title "Temp. Santiago" lt rgb "#406090", \
     "" using 4:xtic(2) with histogram title "Temp. Pto Montt" lt rgb "#40FF00"

set yrange [-10:20]
set xrange [0:8]
set datafile separator ","
set style data lines
plot "temps_ciudades.csv" using 0:($3-$4):xtic(2) with line title "Diferencias de temperaturas"
unset multiplot
\end{verbatim}

Resulta con el gráfico combinado de barras y de diferencias.

\url{./ejercicios/multiplot\_temp.png"}

Guía Multiplots : \url{http://www.gnuplotting.org/tag/multiplot/}
\end{enumerate}


\subsection{Creando una gráfica de frecuencias.}
\label{sec:orgc4ffbc7}

Tenemos el archivo \textbf{seleccion.psv} que dispone la frecuencia de las palabras por
cuento. 

Seguramente habrán palabras que se repiten. Por lo que sería bueno reordenar el
archivo y disponer la cantidad de cada cuento por columnas. Además agregar el total.


\begin{verbatim}
BEGIN{
    FS="|";
    OFS=",";
}
(NR>1){
    nombre=$1;
    palabra=$2;
    frecuencia=$3;
    array[palabra][nombre]=frecuencia;
    nombres[nombre]=1;
}
END{
    printf "palabra,";
    for (nombre in nombres){
        printf nombre","
    }
    printf "\n";
    for (palabra in array){
        printf palabra","
        for (nombre in nombres){
            f=array[palabra][nombre];
            frecuencia=f==0?"0":f;
            total+=f;
            printf "%d,", f;
        }
        printf total
        printf "\n"
        total=0;
    }
}
\end{verbatim}

Luego, ejecutamos el script y lo ordenamos con \textbf{sort}

\begin{verbatim}
awk -f reordena_seleccion.awk conteo_palabras.psv | less |sort -n -k10 -r -t','|less
\end{verbatim}

Podemos enviar este resultado a un archivo "tabla\_palabras.csv", con esto
haremos lo siguiente.

Un \textbf{script} que reciba una serie de palabras. Busque estas palabras en
\textbf{tabla\_palabras} y grafique la frecuencia de esta serie de palabras en un
gráfico de barras compuesto.

\begin{verbatim}
#! /bin/bash
palabras=$(echo "$@" | tr [:upper:] [:lower:] | sed 's/[[:blank:]]/\n/g' | sort | uniq)
archivo=tabla_palabras.csv
for palabra in ${palabras[@]}; do
    grep --color "^$palabra," $archivo
done
\end{verbatim}

¿Cómo se puede crear el código de \textbf{gnuplot}?

\section{Grafos y Diagramas}
\label{sec:org6c91885}

Otra herramienta para poder visualizar ideas es  \textbf{\textbf{graphviz}}. Se considera un
lenguaje que permite describir conexiones entre objetos. Realiza la
visualización de un código y permite otorgar atributos y estilos, como forma de
figura, tamaño, colores.

El lenguaje que implementa \textbf{graphviz} se llama \textbf{dot}. Es un software que además
puede ser utilizado desde diversos lenguajes como \textbf{java}, \textbf{python} y otros. 

Visualizar grafos puede ser una herramienta de utilidad para crear software
comprensible. 

\subsection{Keywords}
\label{sec:org4dc9164}

Las siguientes palabras forman parte de la síntaxis de \textbf{dot}

\begin{itemize}
\item node
\item edge
\item graph
\item digraph
\item subgraph
\item strict
\end{itemize}

\subsection{Primer grafo}
\label{sec:orgce0e4cf}

Conectamos "Hola" con "Mundo". Se forman dos \textbf{globos} conectados con una flecha.

\begin{verbatim}
digraph G {Hola->Mundo}
\end{verbatim}



\section{Literatura recomendada}
\label{sec:org5ad2b02}

\url{https://www.ingeniovirtual.com/tipos-de-graficos-y-diagramas-para-la-visualizacion-de-datos/}


\begin{itemize}
\item GNUPlot in action, de Philipp K. Janert
\item GNUPlot coockbook, Lee Phillips
\item GNUPlot An Interactive Plorring Program, Thomas Williams \& Colin Kelley
\end{itemize}
\end{document}
