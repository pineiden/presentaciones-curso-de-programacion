#+LATEX_COMPILER: lualatex
#+TITLE: Lenguaje para el escaneo y procesamiento de AWK V
#+AUTHOR: David Pineda Osorio
#+LATEX_HEADER: \usepackage[spanish]{babel}
#+LANGUAGE: es
#+OPTIONS: H:2 toc:1 num:f
#+OPTIONS: ^:nil
#+latex_header: \usepackage{setspace}
#+latex_header: \usepackage{fontspec}
#+latex_header: \onehalfspacing


* Temario de la clase

** ¿Qué veremos en esta clase?

- Tarea de Geometría :: tarea para poner en práctica los conocimientos.

- Práctica de awk :: Uso práctico y completo del lenguaje.

- Variable de estado :: FNR, número de fila por archivo.

- Uso de varios archivos :: Se enseñará el uso de varios archivos en que se
     puede combinar información entre ellos.

* Registrar inicio de clases

** Una marca en la línea de comandos: con nombre de clase

Para poder realizar una búsqueda que tenga un mayor contexto y precisión, 
es posible añadir más texto a la seña que nos permita mejorar la referencia.

Recomiendo añadir, al número de la clase, la seña relacionada al nombre de la
clase, que en este caso sería *awk_05*.

#+BEGIN_SRC bash
echo "CLASE_16:awk_05"
#+END_SRC

Anota en tu cuaderno la fecha y la seña *CLASE_16::awk_05*
     
* Proyecto de Geometría

Este proyecto consiste en aplicar los conocimientos del compartidos a lo largo
del curso. También considera la aplicación del /método científico/ para recolectar
datos, aplicar criterios de selección y ordenamiento.

Consiste en establecer una metodología que debe ser similar en los cuatro casos
contemplados, por lo que es importante enfocarse en realizar bien un caso ya que
el resto se debería hacer de la misma manera.

En este ejercicio será necesario que crees carpetas y archivos, guardes datos de
manera ordenada, programes funciones y apliques un script a los datos ordenados
para realizar algunos cálculos.
  
** Enunciado, parte I

- Abrir la carpeta  *geometria*, que contiene cuatro paǵinas (imágenes), una para tríangulos, otra para 
cuadriláteros (paralelogramo), otra para paralelepípedos, otra para cilindros.

Cada página contiene figuras en que se especifican los valores de
interés para obtener (área, volumen y superficie) y las fórmulas asociadas a cada caso.
Hacer lo siguiente:

- Por cada tipo de figura crear un archivo *csv*, cuyo nombre sea *geo_TIPOFIGURA.csv*; que contenga como campos {id, parámetro1, parámetro2, etc}.
En cada caso serían:

|----------------+----------------------+----------------------|
| <10>           | <20>                 | <20>                 |
| *Figura*       | *parámetros*         | *valores pedidos*    |
|----------------+----------------------+----------------------|
| tríangulo      | {base, altura}       | área y perímetro     |
| cuadrilatero   | {ancho, alto}        | área y perímetro     |
| paralelepipedo | {ancho, largo, alto} | volumen y superficie |
| cilindro       | {radio, altura}      | volumen y superficie |
|----------------+----------------------+----------------------|

** Enunciado, parte II


La tarea consite en tomar los parámetros de cada figura y *calcular* los
/valores pedidos/.   

- En un archivo ejecutable de bash *(programa_geo.sh)* escribir el código necesario para llevar a cabo la tarea. 

- Programar en *awk* las funciones que permitan tener los valores que necesitamos {área, perímetro, volumen y superficie}

- Realizar la lectura de todos los csv y en otro programa *awk* entregar un reporte que se guarde en un archivo *geo_REPORTE.csv*,
con un separador *;*, que contenga los campos {id, tipo_figura, área, perímetro, volumen, superficie}

- Ayuda :: Dividir para reinar. Es decir, un problema complejo lo puedes abordar
           por partes

** Guía gráfica

Para realizar este proyecto te será necesario seguir un flujo de trabajo que se
muestra en la figura [[tarea_geom]]. Como es evidente, no podrás generar un reporte
final si no logras ordenar adecuadamente los datos y proecesarlos con las
funciones y scripts que programes.
   
#+CAPTION: Guía para realizar Proyecto de Geometría
#+NAME: tarea_geom
#+ATTR_HTML: :alt guía geometría awk :title Guía para realizar Proyecto de Geometría :align center                                                                          
#+ATTR_HTML: :width 250px   
#+ATTR_ORG: :width 250px
#+ATTR_LATEX: :width 250px
[[file:./img/geometria_guia.png]]          

* Programar en ~awk~

El siguiente esquema [[pasos]] es el proceso de *reflexión* que podrías llevar antes de
crear un programa en *awk* o cualquier otr lenguaje (incluso). Por lo que es
bueno llevarlo a la práctica. 

#+CAPTION: Pasos de un programa en awk
#+NAME: pasos
#+ATTR_HTML: :alt pasos programa awk :title Pasos para programar en awk :align center
#+ATTR_HTML: :width 250px   
#+ATTR_ORG: :width 250px
#+ATTR_LATEX: :width 250px
[[file:./img/tecnica_awk.png]]

* Webscraping, descargar libros desde UNAM

  La palabra /webscraping/ hace referencia a obtener información mediante la
  selección de esta que se encuentra disponible en páginas webs. Es decir de
  toda una página que se encuentra disponible en el browser, se puede filtrar y
  seleccionar aquella que pueda corresponder a archivos, imágenes o lo que
  necesites.

  Para eso necesitas saber, en primer lugar, que una web se visualiza porque
  está escrita en *texto estructurado* en etiquetas. Es decir, atrás de lo que
  vez hay texto que dice que un menú va arriba, el contenido al centro, las
  imágenes a la derecha, etcétera.
  
** Rasguñar desde la red

En la biblioteca de la *Universidad Autónoma de México*,(/UNAM/) se han
digitalizado bastantes textos que son de interés público.

- URL :: [[http://www.historicas.unam.mx/publicaciones/publicadigital/publicadigital.html][Biblioteca Digital UNAM]]

Este ejercicio consiste en que, dada una *url* de algún libro en particular,
crear un *script* que descargue todos los *pdf* relacionados con el libro.
Utilizando todos los comandos posibles aprendidos hasta el momento.

Además, se recomienda usar un nuevo comando ~curl~, que se utiliza para
descargar el código *html* de la página dada una dirección.

#+BEGIN_SRC bash
sudo apt install curl
#+END_SRC
 
** Descargar libro

El título del libro es  /'Apariciones de Seres Celestiales y Demoniacos en Nueva
España'/. Se encuentra en esta [[http://www.historicas.unam.mx/publicaciones/publicadigital/libros/apariciones/celestiales.html][url]] y cada sección está disponible en un *pdf*
por separado. Es decir, hay un /conjunto de archivos/ que componen el /libro/.

La misión es:

- descargar la web
- seleccionar las url de archivos *pdf*
- descargar los *pdf*
- unirlos de manera ordenada.

Para ver una *página web* en la *terminal* será necesario descargarla de esta
manera.

#+BEGIN_SRC bash
base_url=http://www.historicas.unam.mx
entremedio=/publicaciones/publicadigital/libros/
libro=apariciones/celestiales.html
web_libro=$base_url$entremedio$libro
ruta_libro=$(echo $web_libro|
    awk -F'/' '{OFS="/";$(NF--)=""; print}')
nombre=$(echo $web_libro|
    awk -F'/' '{OFS="/";$(NF--)=""; print $(NF-1)}')
echo $ruta_libro
mkdir $nombre
curl $web_libro
#+END_SRC

Y, por último, para descargar la web y poder procesarla como un texto. Se
descarga y almacena la información en un archivo.

#+BEGIN_SRC bash
curl $web_libro > web/libro.html
#+END_SRC

O bien, usando directamente *curl* pra guardar el archivo.

#+BEGIN_SRC bash
curl --create-dirs -o web/libro.html $web_libro
#+END_SRC

Con esto tendremos el archivo *html* en nuestro disco duro listo para ser
analizado y filtrado.

** Limpiar y separar

Esta tarea supone generar una hipótesis respecto al sitio web. 

- Es un sitio web estándar
- En cada ítem o libro se mantiene el formato o estructura 
- Es posible descargar directamente los archivos

La información que podemos observar:

- Los archivos son, en este caso con extensión *pdf*
- Se ocupa una estructura de etiquetas *html*

Revisamos el archivo:

#+BEGIN_SRC bash
cat libro.html
#+END_SRC

** Obtener las líneas que contengan una extensión ~pdf~

Se filtra, utilizando grep. Ya que son archivos *pdf*, deberían tener la cadena
en alguna parte. Podemos filtrar con *grep*.

#+BEGIN_SRC bash
grep pdf libro.html
#+END_SRC

** Filtrar dos veces con ~awk~

Vemos que, para la estructura *html*, un enlace a un archivo se da mediante la
etiqueta *a*. Tiene una forma muy característiaca, es decir un *patrón*.

De este patrón nos podemos tomar para filtrar y obtener adecuadamente el enlace.
   
Fijándose en la etiqueta *<a href=* y la extensión *.pdf*.

#+BEGIN_SRC bash
grep pdf libro.html | 
  awk -F'<a href="' '{print $2}'
#+END_SRC

Luego, se encadena un segundo filtro

#+BEGIN_SRC bash
grep pdf libro.html | 
  awk -F'<a href="' '{print $2}'|
  awk -v path=$ruta_libro -F'.pdf' '{print path$1".pdf"}'
#+END_SRC

** Descargar la lista de libros ~pdf~

Se utiliza el comando ~wget~, que permite descargar archivos, entregándole una
ruta al archivo remoto.

#+BEGIN_SRC bash
grep pdf libro.html | 
  awk -F'<a href="' '{print $2}'|
  awk -v path=$ruta_libro -F'.pdf' '{
         print path$1".pdf"}' |
  xargs wget -P $nombre
#+END_SRC

** Poner a prueba

La misión es crear un *script* que contenga cada paso y permita descargar
distintos libros desde la plataforma de la UNAM.

Probar que, para distintos libros, es posible descargar todos los archivos que
le contienen.

¿Se cumple la hipótesis?

Si así es, el script está correcto.

¡Comparte tu programa con tus amistades!


* El uso de múltiples archivos

En el momento de relacionar información de fuentes distintas, sean tablas o
archivos de diversa índole, se hará necesario tener alguna forma de
/comparación/ y /enlace/.

En *awk* es posible trabajar con muchos archivos a la vez, pudiendo realizar
tareas combinadas entre todos. Para eso aprenderemos como se puede hacer
partiendo con dos archivos.

** Inspección de un programa con varios archivos

La idea de este ejercicio es conocer *profundamente* como poder trabajar con
varios archivos, la información que contienen y la (información) que el lenguaje
~awk~ destila al *escanear* cada archivo.

La estructura de un programa ~awk~ con varios archivos es de la manera
siguiente.

#+BEGIN_EXAMPLE
awk '{programa}' archivo1 archivo2 ... archivoN
#+END_EXAMPLE

Conoceremos, experimentalmente cómo funciona la lectura de varios archivos con
~awk~

El *intérprete awk* parte leyendo cada archivo de manera ordenada y, a cada
línea, la procesa con el '{programa}'.

** Crear dos archivos con unas pocas líneas

Para comenzar, crearemos dos archivos con frases.

Archivo 1

#+BEGIN_EXAMPLE
Esta maravilla del instante
Los mejores lugares del universo
#+END_EXAMPLE

Archivo 2

#+BEGIN_EXAMPLE
Están plantados con horizontes fastuosos
El impoluto martín pescador
Se sumerje sobre su sombra
#+END_EXAMPLE

** La variable de estado ~FNR~ y ~NR~

Cuando controlamos varios archivos en el mismo script será necesario conocer el
uso de la variable ~FNR~.

Ahora solo visualizaremos las líneas completas de ambos archivos, indicando el
valor de ~NR~ que ya conocemos, que indica el número de línea del total de
líneas leídas, y de ~FNR~ que indica el número de línea del
archivo que se está leyendo.

Mostramos como cambian ambos valores frente a la lectura de cada archivo.

#+BEGIN_SRC bash
awk '{print "NR->"NR,"FNR->"FNR,$0}' archivo1 archivo2
#+END_SRC

** ¿Qué pasa con un tercer archivo?

Añadimos un tercer archivo, que contenga lo siguiente.

#+BEGIN_EXAMPLE
Alimentar el cuerpo frágil
Elevarse tras la cortina de agua
Elevarse entre dos medios
Cargar en el cuerpo otro cuerpo
Agotado de vida
#+END_EXAMPLE

El mismo comando, extendido

#+BEGIN_SRC bash
awk '{print "NR->"NR,"FNR->"FNR,$0}' archivo1 archivo2 
                                     archivo3
#+END_SRC

Funciona correctamente, entonces

- ¿Cuál es la diferencia entre *NR* y *FNR*?

** La acción ~next~

La acción ~next~ será útil para controlar la lectura de las líneas en el mismo
archivo, por lo que el uso práctico es cuando usamos varios archivos.

La /sentencia/ *next* detiene la operación de la línea que está operando para
continuar con la siguiente.

Por ejemplo:

Un programa que pase a mayúsculas solo aquellas frases con más de 3 palabras.
También, cada una de las frases no seleccionadas debe mostrar la última palabra al revés.

Archivo de ejemplo, llamarlo *frases*:

#+BEGIN_EXAMPLE
Cuatro caballeros
El último día miro hacia el cielo azul
Un tigre, dos tigres, cuatro tigres
Perlas abonadas sobre la tierra
Pan con tomate
La ciudad infernal quedó vacía
#+END_EXAMPLE

En un script *awk* llamado *manipula_frases.awk*

Primero, creamos la función que retorna el texto al revés.

#+BEGIN_SRC  awk
function reverse(texto){
verres=""
for(i=length(texto);i>=1;i--){
verres=(verres substr(texto,i,1))
}
return verres
}
#+END_SRC

Luego escribimos el script que haga lo solicitado.

#+BEGIN_SRC awk
{
if(NF>3){
print toupper($0);
next
};
verres=reverse($NF);
print verres
}
#+END_SRC

Finalmente, ejecutamos el script.

#+BEGIN_SRC bash
awk -f manipula_frases.awk frases
#+END_SRC 

Tendremos una salida como esta.

#+BEGIN_EXAMPLE
sorellabac
EL ÚLTIMO DÍA MIRO HACIA EL CIELO AZUL
UN TIGRE, DOS TIGRES, CUATRO TIGRES
PERLAS ABONADAS SOBRE LA TIERRA
etamot
LA CIUDAD INFERNAL QUEDÓ VACÍA
#+END_EXAMPLE

** Dos archivos con condición (FNR==NR)

Cuando ya tratamos con varios archivos, podemos controla *cuando* un trozo de
código solamente procesa uno y no otro. Para eso debemos usar los condicionales
y las variables de estado conocidas.

Si ponemos la condición *(FNR==NR)* y añadimos el segundo archivo.
Debería mostrar una vez cada línea del primer archivo, una vez del segundo.

#+BEGIN_SRC bash
awk '(FNR==NR){
print "Archivo 1: NR->"NR,"FNR->"FNR,$0}
{
print "Archivo 2: NR->"NR,"FNR->"FNR,$0
}' archivo1 archivo2
#+END_SRC 

¡Algo nos falta! Que nos permita hacer que lo que se haga para el primer archivo
solo ocurra en la primera sección y no la segunda. Usaremos ~next~. Así nos
evitaremos que las líneas del primer archivo se procesen con dos trozos de
código, cuando no debería.

** Dos archivos con ~next~

Si añadimos el segundo archivo y la acción ~next~.

Debería mostrar una vez cada línea del primer archivo, una vez del segundo.

#+BEGIN_SRC bash
awk '(FNR==NR){
print "NR->"NR,"FNR->"FNR,$0; 
next}
{
print "NR->"NR,"FNR->"FNR,$0;
}' archivo1 archivo2
#+END_SRC 

La acción ~next~ permite que el código del segundo paréntesis *no* se ejecute.
Lo que ocurre, es que una vez se acciona ~next~ el lenguaje ~awk~ lo entiende
como una orden para pasar a la siguiente línea.

** Crear un control del número de archivo.

De otra manera, si deseamos ejecutar cierto trozo de código para un archivo
específico, deberíamos crear un control de posición del archivo actual,
crearemos una *variable de estado* relacionada al archivo leído.

Cada vez que empieza a leer un nuevo archivo, le decimos que este parámetro
contador aumenta en 1.

#+BEGIN_SRC awk
(FNR==1){
archivo_nro+=1
}
(archivo_nro==1){
print "Primer archivo",$0}
(archivo_nro==2){
print "Segundo archivo", $0}
(archivo_nro==3){
print "Tercer archivo", $0
}
#+END_SRC

Es un programa llamado *selector.awk* que, simplemente, lee de manera selectiva cada archivo y muestra
la línea respectiva a cada archivo.

** Correr el script selector de archivo

Siendo el script nombrado *selector.awk*, se ejecuta.

#+BEGIN_SRC bash
awk -f selector.awk archivo1 archivo2 archivo3
#+END_SRC

** Comparar información contenida en distintas tablas

En este ejercicio debemos tomas información del archivo /comparar/ciudades.csv/ y encontrar todo
lo relacionado con cada /tupla/ de este en el archivo /comparar/poblacion.csv/. Con esto buscaremos
la cantidad de habitantes por país total de las ciudades que tenemos registradas.

Listar país y población

#+BEGIN_SRC awk
(FNR==NR && NR>1){
ciudad[$1]=$1;
cname[$1]=$3;
pais[$1]=$2; 
next}
($1==c[$1]){
print ciudad[$1], pais[$1], $2
}
#+END_SRC

Si el script es *listar.awk*, observar el uso de dos archivos.

#+BEGIN_SRC bash
awk -F',' -f listar.awk  ciudades.csv poblacion.csv
#+END_SRC

** Resumen del total de cada país.

Debería ser, un script que recopile información y muestre al final.

#+BEGIN_SRC awk
BEGIN{OFS=";"}
(FNR==NR && FNR>1){
	ciudad[$1]=$1;
	pais[$1]=$2;
  cname[$1]=$3;
# definimos un map o hash que dará el total habitantes
	total[$2]=0;
	next
}
($1==ciudad[$1]){
# sumamos el habitante por ciudad al 
# elemento correspondiente por país
  pais=pais[$1];
	total[pais]+=$2
}
#+END_SRC

** El cierre del script "Resumen del total"

Añadimos el cierre del programa, como *END*

#+BEGIN_SRC awk
END{
	for (pais in tot){
		print pais,total[pais]
	}
}
#+END_SRC

Si llamamos al script *resumen.awk*, se ejecutaría.

#+BEGIN_SRC bash
awk -F',' -f resumen.awk ciudades.csv poblacion.csv 
#+END_SRC

** Reglas de inicio y final de archivo.

Así cómo para un programa *awk* tenemos disponible las secciones BEGIN y END,
que ocurren cuando no se esa procesando el archivo, sino antes y después de
procesar la información. Puede darse el caso que necesitemos hacer algo *antes* o *después* de 
leer cada archivo. Para eso están las secciones especiales BEGINFILE y
ENDFILE.

Por ejemplo, imprimir el /nombre del archivo/ al iniciar la lectura y un mensaje
de fin de archivo. O el tiempo que ha tomado leer y procesarlo.

Por ejemplo, para mostrar *FILENAME*, la variable que indica el nombre del
archivo. En el ejemplo *manipula_frases*.

#+BEGIN_SRC awk
BEGINFILE{print "El archivo activo es: "FILENAME}
{
if(NF>3){
print toupper($0);
next
};
verres=reverse($NF);
print verres
}
ENDFILE{print "-----FIN DE ARCHIVO------"}
#+END_SRC

* Estudio recomendado.

** The AWK Programing Language

En el libro *The AWK Programing Language*, de *Aho, Weinenberg y Kerninghan*,
los autores del lenguaje. Estudiar y realizar los ejemplos y ejercicios que se
encuentren en el texto.

Por ejemplo:

- Algoritmo Quicksort :: para ordenar

** Otras sentencias de control

Dentro de los /loop/ como *for* o *while* es posible controlar la dentención o el
salto del procesamiento de esta iteración.

Para eso disponemos de los siguientes dos elementos:

- continue :: se usa cuando deseamos saltar al siguiente valor de iteración y no
  procesar lo que sigue en el código. Ver [[https://www.gnu.org/software/gawk/manual/html_node/Continue-Statement.html][documentación]]

- break :: se usa para detener de forma absoluta el /loop/. Ver [[https://www.gnu.org/software/gawk/manual/html_node/Break-Statement.html][documentación]]

También, por otro lado, cuando tienes muchos casos diferentes para un *if*, es
posible escribir un *switch* contemplando cada caso. Ver [[https://www.gnu.org/software/gawk/manual/html_node/Switch-Statement.html][documentación]]

** Funciones de tiempo

*AWK* provee algunas funciones para la manipulación y procesamiento de valores
temporales.

- mktime :: retorna una fecha dada en timestamp. Tiene un formato específico de
  "YYYY MM DD HH MM SS [DST]".

- strftime :: dados un formato y el timestamp (por defecto ahora), entrega la
  fecha en un formato definido. Sigue el estándar C para formateo de fechas.

- systime :: retorna el timestamp entregado por sistema.

Ver [[https://www.gnu.org/software/gawk/manual/html_node/Time-Functions.html][documentación]]

* Ejercicios

** Operar con archivos

1.- Buscar los archivos *png* en el directorio *./ejercicios/imagenes* (y en sus subdirectorios). Cambiar
el nombre que tengan a minúsculas y sin espacios.

** Uso de templates y tags

Se tienen los templates en la carpeta *./ejercicios/tempsvg* de las figuras svg
{circulo, triangulo y cuadrado}, encontrar sus parámetros y entregar archivos
por color {rojo, verde, azul} y por tamaño de radio o lado. Los patrones a
cambiar son:  

- circulo -> [RADIO] y [COLOR]
- triángulo -> [COLOR]
- cuadrado -> [LADO] y [COLOR]}

Recuerda hacer *cat* sobre cada archivo para estudiar su contenido. 

** Operar con directorios

En la misma carpeta mover las *jpg* a una carpeta llamada *fotos* y comprimir con *tar*.

** Operar con fechas

Buscar los archivos *jpg* creados durante el año 2017 y moverlos a la carpeta *fotos2017* y las
del 2018 a *fotos2018*.

** Operar CSV y usar funciones definidas

Trabajar con un archivo CSV obtenido de DataChile

1. traducir lo que tenga en inglés
2. Extraer lo que tenga relación con América y África. 
3. Todo lo que esté en Kg pasarlo a Libra
4. La moneda que esté en peso a dolar.

** Utilizar variables de ambiente

Se tiene la variable de posición en la terminal *$(pwd)*, y un archivo que contiene una llave
serial en ejercicios, crear un CSV con las rutas de los archivos odt, ods, odp y una columna con el valor de la llave. 

Recuerda que, para exportar los valores de ambiente.

#+BEGIN_SRC bash
export PWD=$(pwd)
echo "Esta es la posicion "$PWD
export LLAVE=$(<SERIAL_KEY)
echo "Esta es la llave secreta "$LLAVE
#+END_SRC
