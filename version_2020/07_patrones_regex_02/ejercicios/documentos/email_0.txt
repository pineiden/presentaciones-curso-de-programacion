para: alejandra@defensoriaambiental.org
de: dpineda@uchile.cl

Amiga Ale.

¡Me acabo de enterar de la noticia! De todo corazón las felicito por todo el trabajo, lucha y empeño que
han puesto defendiendo a las comunidades.

Quedó en el aca CC-2019/08-03 que desde el curso podríamos hacer un proyecto de apoyo a las mismas, espero
que quienes participen se animen a aportar en la red de los comunes.

Un abrazo
