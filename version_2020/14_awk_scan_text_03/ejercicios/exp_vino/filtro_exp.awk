#! /usr/bin/awk -f

BEGIN{FS=","}
{
if($2 ~ /Europa/){
	print $0
}
else if ($2 ~ /Asia/){
	print $4
}
else{
	print "NO"
}
}
