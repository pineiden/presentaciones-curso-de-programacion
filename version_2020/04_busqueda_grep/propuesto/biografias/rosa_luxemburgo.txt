Rosa Luxemburgo [1871-1919]
==========================


La mujer cuya vida y obra nos inspira fue una de las grandes revolucionarias del
 siglo XX y una de las fundadoras de la corriente de pensamiento del socialismo 
democrático.

Hija de un comerciante de Varsovia, su brillante inteligencia le permitió estud
iar a pesar de los prejuicios que imperaban contra las mujeres en ese entonces, 
y pese a la discriminación antisemita que existía en Europa contra los judíos
. Rosa Luxemburg hizo un doctorado en una época en la que poquísimas mujeres i
ban a la universidad. Se dice que hablaba once idiomas. Pronto destacó como una
 de los principales dirigentes de la socialdemocracia europea.

En 1889, a los 18 años, abandonó Polonia a consecuencia de la persecución de 
la policía debido a su militancia socialista, refugiándose en Suiza. Allí ter
minó sus estudios, entró en contacto con revolucionarios exiliados y se unió 
a la dirección del joven Partido Socialdemócrata Polaco. Contrajo matrimonio e
n 1895 con Gustav Lübeck para adquirir la nacionalidad alemana y poder trabajar
 con el movimiento obrero en este país.

Junto al político alemán Karl Liebknecht, fundó la liga de Spartacus, que má
s adelante se convertiría en el Partido Comunista Alemán. Fue redactora del pe
riódico teórico marxista “Neue Zeit” y autora de varios libros. Fue senten
ciada (1903-1904) a nueve meses de prisión acusada de “insultar al Kaiser” 
(emperador). Participó directamente en la revolución de 1905 en Polonia. En ma
rzo de 1906 fue arrestada y encarcelada en Varsovia durante cuatro meses.

Participó activamente tanto en el Congreso del partido socialdemócrata alemán
 en 1906 como en el Congreso Socialista Internacional celebrado en Stuttgart un 
año después, en el que intervino en nombre del partido ruso y polaco. Su pensa
miento representaba a las opciones más radicales en el seno de la II Internacio
nal. Gran teórica, realizó importantes contribuciones al desarrollo del marxis
mo, en especial en lo referente a las relaciones entre nacionalismo y socialismo
, y  sobre el socialismo democrático.

Hizo también aportes teóricos originales en torno al imperialismo y al derrumb
e del capitalismo, en su obra “La acumulación del capital” de 1913. Su crí
tica a Marx se basa en las predicciones de éste acerca de las crisis cíclicas 
del capitalismo. Marx pensaba que el capitalismo, como sistema económico y pol�
�tico basado en el crecimiento y la búsqueda constante del beneficio, debía co
lapsar en algún momento, por saturación. Sin embargo, muchas décadas después
 de muerto Marx, las crisis periódicas del capitalismo parecían aplazarse o so
lventarse sin producir convulsiones en el sistema. Rosa Luxemburgo encontró la 
explicación a este hecho en el colonialismo, hallando que el crecimiento de las
 potencias capitalistas encontró una vía de expansión en las colonias, la cua
les, al tiempo que procuraban materias primas a muy bajo costo, servían tambié
n de mercado donde colocar los productos manufacturados. En el mismo sentido, ex
puso las primeras teorías sobre el imperialismo, que más tarde desarrollaría 
Lenin. Rosa Luxemburg creía en una opción socialista internacional, esto es, a
lejada de particularismos y nacionalismos, en la que las masas obreras, solidari
amente, tomaran el poder.

Lenin también fue objeto de críticas por parte de Rosa Luxemburg, en especial 
en lo referente a las concepciones que tenía sobre la democracia en el partido 
y la dictadura del proletariado. Rosa Luxemburg postulaba un menor dirigismo y u
na mayor integración de las bases en la dinámica partidista, y se oponía a la
 concepción del “centralismo democrático” de un partido de revolucionarios
 profesionales que defendía Lenin.

Al estallar la 1ra Guerra Mundial en 1914, el grupo parlamentario socialdemócra
ta alemán (SPD) apoya unánimemente a los créditos de guerra. Rosa Luxemburg, 
pacifista convencida, forma parte de la oposición interna en el SPD, que difund
e centenares de miles de folletos para movilizar a la población contra la guerr
a. Ella es arrestada de nuevo el 20 de febrero, esta vez acusada de incitar a lo
s soldados a la rebelión. Se la sentencia a un año de prisión, pero al salir 
del tribunal se dirige de inmediato a un mitin popular, en el que repite su revo
lucionaria propaganda anti bélica. El conflicto alrededor de los créditos de g
uerra pedidos por el Kaiser para financiar la actividad bélica acaba llevando a
 la escisión del partido en enero de 1917, con la fundación, el 6 de abril, de
l USPD (Socialdemócratas Independientes).

En 1918 hay vientos de revolución en Alemania, cuyas fuerzas de izquierda miran
 hacia el ejemplo ruso y cuya población está cansada de la guerra. El 28 de en
ero se declara la huelga general y se inicia la formación de Consejos Obreros. 
El 31 de enero la huelga es prohibida y se declara el estado de sitio, extendié
ndose la represión. En marzo, Rosa Luxemburg es encarcelada conjuntamente con L
eo Jogiches y otros militantes espartaquistas que difundían propaganda revoluci
onaria en el ejército. El 9 de noviembre, a raíz de un levantamiento de marino
s en Kiel, estalla la “Revolución de Noviembre” con la conformación de Con
sejos de Obreros y Soldados en todo el territorio nacional. El emperador Guiller
mo II abdica. Se pretende la refundación de Alemania como democracia socialista
 con una nueva Constitución. Rosa Luxemburg, liberada dos días antes, llega a 
Berlín y coedita “Bandera Roja”, el periódico de la liga de Spartacus, con
 Karl Liebknecht, para poder influir a diario en los sucesos políticos. En los 
últimos días del año 1918, participa en la fundación del Partido Comunista A
lemán, KPD. Sin embargo, las fuerzas radicales de izquierda no logran imponerse
 frente a la tendencia reformista del socialdemócrata Friedrich Ebert.

El 15 de enero 1919, Rosa Luxemburg y su coideario Karl Liebknecht son asesinado
s en Berlín por los soldados que reprimen el levantamiento. Sus cuerpos son arr
ojados a un canal. Estos asesinatos desatan una ola de protestas violentas en to
do el país, que se extienden hasta mayo 1919, y cuya represión militar lleva a
 varios miles de muertos.


Fuente :  https://www.rosalux.org.ec/biografia/
