#+TITLE: Tarea para afirmar conocimientos: strings, regex, sed, grep
#+AUTHOR: David Pineda Osorio

* Introducción

Ya estando a la mitad del curso, cada estudiante tiene una serie de herramientas
que le permiten buscar, editar y recortar textos dentro de un conjunto de
documentos.

Esta tarea está pensada a modo de ejercicio general, en que puedan aplicar sus
conocimientos.

Cada uno de los estudiantes tiene un archivo particular.

En la carpeta *./ejercicios/tareas* hay una lista de archivos que se
corresponden al nombre de cada uno de los estudiantes y la cantidad de tareas
que se les asignará.

Además, contiene los archivos generadores siguientes:

- Plantilla
- Script generador
- Lista de nombres y cantidad de tareas

Antes de comenzar a buscar y realizar estos sencillos ejercicios será necesario
que reemplaces los espacios *\s* por saltos de línea *\n*. Luego aplicar las
expresiones regulares necesarias. 

Se ha generado una lista de estudiantes (histórico) y la cantidad de tareas que
debe resolver. Guardar la lista en un archivo *lista.csv*.

#+BEGIN_EXAMPLE
nombre, cantidad
mw, 3
Javi, 4
alexeym746, 6
Tsaamaaxan, 3
Giordano, 2
Joaqo12, 5
Palivioleta, 3
ValentinaRojas, 5
kcosgrove, 4
xolotl, 5
Gabriel, 2
Alex_K, 6
fsudaka, 2
Crispr, 5
LaLambo, 6
DVSkywalker, 3
miadelcuy, 2
Hans, 7
Ratonovic, 4
balamaqab, 3
Ipnv, 3
elneu, 5
Sakura, 4
Alee_pineda, 3
madmax, 3
antunahuel, 4
Valentina.Jara, 3
35t3b4n,3
sparrac,4
Jatali,2
keyrex,5
stephanie1991,3
rgscl@yahoo.com,3
NIC,5
jotaxpe,6
#+END_EXAMPLE

También, se tiene un archivo ~txt~ de *plantilla base*, que se llenará especialmente
para cada estudiante.

#+BEGIN_EXAMPLE
El profesor David, está preocupado de que XXX estudie y prepare la sesión
del curso de programación.

Esto es porque al final del ciclo deberá preparar una tarea de YYY preguntas.

XXX ¿Estás preparado para la fecha DATE?
#+END_EXAMPLE

Y, por último, el siguiente es el código que permite, mediante un comando, generar
cada archivo. Que está hecho con el lenguaje ~awk~ que aprenderemos en las
próximas clases.

#+BEGIN_SRC bash
cat lista.csv |awk -F',' '{
gsub(/ /, "", $1);
gsub(/ /, "", $2);
cd="date -u +\"%D %H:%M:%S\"  -d \" +1 month\"";
cd|getline datec;
file_out=$1".txt";
command1="sed -e \"s/XXX/"$1"/g\" texto.txt|sed \"s/YYY/"$2"/g\">"file_out;
system(command1);
com_date="sed -i \"s#DATE#"datec"#g\" "file_out;
system(com_date);
print command1, $1, $2}'
#+END_SRC

* Ejecutar el comando.

Genera la lista de archivos y selecciona el que te corresponda.

#+BEGIN_SRC bash
bash COMMAND.sh
#+END_SRC

* Una tarea de textos, fechas y regex para cada uno

En el archivo que te corresponda, realizar las siguientes acciones.

** Obtener todos los números que se encuentren en el archivo de texto

Esto quiere decir que hay que encadenar a una búsqueda de una *regex* que nos
permita encontrar coincidencias numéricas en esa línea (considerando que cada
línea es una palabra del archivo). 
 
** Obtener todas las palabras que comiencen con mayúscula

Esto quiere decir que necesitamos encontrar las palabras de comienzo de párrafo
o preguntas. Para eso necesitamos componer una *regex* que considere la
posibilidad de que al principio tenga un signo *?*. 

** Mostrar todas las palabras que empiecen con la misma letra de su nombre

Por ejemplo, si tu nombre comienza con *d*. Deberías construir una regex que
considere la posibilidad de una palabra que comience con *d* o *D*. Recordando
que el uso de  $"\string^\string^"$ (gorrito) es necesario para detectar patrones que comiencen con
la expresión.

** Mostrar las preguntas del texto

Es necesario que definas una *regex*, pensando que:

- Las preguntas comienzan con el carácter "¿"
 
- Las preguntas cierran con el carácter "\?"

- Entre los signos hay palabras, pueden ser preguntas de varias líneas inclusive.

Se recomienda el uso de *sed*, ya que permite mostrar todas aquellas líneas que
se encuentran entre dos coincidencias.

#+BEGIN_SRC bash
sed -n -e "/¿/,/\?/p" MaríaJose.txt
#+END_SRC

Recordando que "$?$" debe ir desactivado "$?$", ya que es un operador de las
*regex*. Y debemos tomarlo solo como carácter.

