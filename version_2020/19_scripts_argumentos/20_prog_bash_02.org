#+LATEX_COMPILER: lualatex
#+TITLE: Bash avanzado: permisos y scripts con argumentos III
#+AUTHOR: David Pineda Osorio
#+LATEX_HEADER: \usepackage[spanish]{babel}
#+LANGUAGE: es
#+OPTIONS: H:2 toc:1 num:f
#+OPTIONS: ^:nil
#+latex_header: \usepackage{setspace}
#+latex_header: \usepackage{fontspec}
#+latex_header: \onehalfspacing


* Temario de la clase

** ¿Qué veremos en esta clase?

- Permisos de usuario y grupo :: para controlar el acceso y ejecución de archivos 

- Crear scripts con opciones con ~getopts~ :: Definir opciones de
     ejecución de un scripts, en base a banderas e información.

- Crear scripts con opciones y uso de $@ :: otra forma de crear scripts con
     opciones, trabajando la entrada como lista.

* Creación de usuarios y grupos

** Listar usuarios y grupos

Existe un archivo en */etc/passwd* que contiene la lista de usuarios registrados
en la computadora. La información de usuario está separada por *:*.

*** ¿Qué significa cada parte?

1. Nombre de usuario
2. x si la password está encriptada en */etc/shadow*
3. ID de usuario 
4. ID de grupo 
5. Información completa de usuario
6. Directorio home
7. Shell de login

** Crear un script ~awk~ que muestre información

Crear un comando que muestre:

- nombre de usuario 
- nombre completo
- grupo de usuario
- directorio home

Hacer una función ~bash~ que se le entregue un *username* y te retorne esa
información.

#+BEGIN_SRC bash
username=david
grep $userame /etc/passwd | awk -F':' '{
 print "username: "$1, 
       "info usuario: "$5,
       "grupo: "$4,
       "ruta a home: "$6}'
#+END_SRC

Sin embargo, no nos sirve solo tener el número, nos servirá tener el nombre.
Continuemos.

** Listar grupos

De manera análoga, para conocer los grupos disponibles en */etc/group*

Para obtener el nombre del grupo, usamos  ~awk~ combinando los archivos.

#+BEGIN_SRC bash
awk -v user=$username -F':' '(FNR==NR && $1==user){
info=$5;
group_id=$4;
home_path=$6}
($3==group_id){
group=$1}
END{
print user, 
      info, 
      group_id":"group, 
      home_path}' /etc/passwd /etc/group
#+END_SRC

** Crear usuarios 

Para crear un usuario, con carpeta en */home* se realiza lo siguiente:

#+BEGIN_SRC bash
sudo adduser amiga
sudo adduser pinpon
#+END_SRC

Pedirá la información necesaria, password y otras.

Si deseas crear un script que automáticamente cree usuarios, debes investigar el
comando *useradd*.

- url :: [[https://www.tecmint.com/add-users-in-linux/][Manual adduser]]

** Cambiar la password

En caso de necesitar una nueva password para un usuario.

#+BEGIN_SRC bash
sudo passwd amiga
#+END_SRC

Recuerda anotarla o memorizarla.

** Modificar usuario
El comando para modificar la información respecto al *usuario*, es *usermod*.

#+BEGIN_EXAMPLE
sudo usermod [OPCION-accion] usuario
#+END_EXAMPLE

Por ejemplo, para el usuario *amiga* añadir al grupo *visita*.

#+BEGIN_SRC bash
sudo usermod -G visita amiga
#+END_SRC

O una cuenta que tenga una duración 

#+BEGIN_SRC bash
sudo usermod -e 2021-01-01 amiga
# revisando
sudo chage -l amiga
#+END_SRC

- url :: [[https://www.tecmint.com/usermod-command-examples/][Manual usermod]]

** Borrar usuario

Para eliminar un usuario

#+BEGIN_SRC bash
sudo userdel amiga
#+END_SRC

Y desaparece del sistema.

** Crear grupo

Para crear un grupo, se debe realizar el comando *groupadd*

#+BEGIN_SRC bash
sudo groupadd revolucion
#+END_SRC

** Modificar grupo 

Utilizando el comando *groupmod*

#+BEGIN_EXAMPLE
sudo groupmod [OPCION-accion] group
#+END_EXAMPLE

Cambiar de nombre

#+BEGIN_SRC bash
sudo groupmod -n comunitario revolucion
#+END_SRC

- url ::
         [[https://www.geeksforgeeks.org/groupmod-command-in-linux-with-examples/][Manual groupmod]]

** Borrar grupo

Para eliminar un grupo

#+BEGIN_SRC bash
sudo groupdel amiga
#+END_SRC

Y desaparece del sistema.

* Permisos de lectura, escritura y ejecución y accesos para archivos.

** Asignar propiedad a un archivo

Existe un comando que permite asignar la propiedad de un archivo o directorio, este es *chown*.

Se utiliza el comando ~chown~ en la forma.

#+BEGIN_EXAMPLE
chown [USER]:[GROUP] archivo_o_directorio
#+END_EXAMPLE

Por ejemplo, si tengo un usuario /pinpon/ y un grupo /muñecos/. Puedo asignar la propiedad
de un archivo de esta manera:

#+BEGIN_SRC bash
chown pinpon:muñecos archivo
#+END_SRC

** Asignar propiedad a un directorio

Para un directorio, es similar, y asignar de manera recursiva a todos los
elementos del directorio.


#+BEGIN_SRC bash
chown -r amiga:comunitario directorio
#+END_SRC

** Permisos de acceso, lectura y ejecución

Si necesitas controlar sobre quien y qué podría hacer un usuario con un archivo específico, será necesario
que le asignes directamente los permisos específicos relacionados al archivo.

Las posibles combinaciones que se pueden asignar están relacionadas con estos tres estados y sus niveles respectivos:

1) De lectura (se representa con la letra r por read).

2) De Escritura (se representa con la letra w por write).

3) De Ejecución (se representa con la letra x por execute).

** Tabla de octales, importante!

La tabla de octales, que permite asignar los permisos de manera general es la siguiente:

#+CAPTION: Tabla de octales
#+NAME:   fig: octales
[[file:./img/permisos_OCTALES.png]]

** Equivalencia

#+CAPTION: Asignación de Permisos a archivos
#+NAME:   fig: permisos
[[file:./img/permisos.png]]

** Calculadora de permisos, en la web

Al principio puede ser un tanto complejo definir bien los permisos.

Para eso hay herremientas disponbile, en la web puedes ver:

[[https://ss64.com/bash/chmod.html][Calcular CHMOD]]

** Asignar permisos a un archivo

De esta manera, es posible con el siguiente comando realizar la asignación:

#+BEGIN_SRC bash
chmod 761 archivo
#+END_SRC

Agrega, según la tabla:

- Todos los permisos a usuario

- Al grupo permisos de lectura escritura, no ejecución

- A los otros usuario, permisos de ejecución 

** Asignar permisos en modo simbólico (a,u,g,o)

La interpretación para cada letra es la siguiente, respecto a usuarios y grupos.

- u :: asignar solo al usuario propietario del archivo
- g :: asignar al grupo propietario del archivo
- o :: otros usuarios que no pertenecen al grupo 
- a :: todos los usuarios

** Acciones posibles en modo simbólico (w,r,x,etc)

Los símbolos más usados:

- w :: permisos de escritura
- r :: permisos de lectura
- x :: permisos de ejecución

** Construcción de un permiso simbólico

La forma en que se asigna un permiso es la siguiente:

#+BEGIN_EXAMPLE
chmod [usuarios-grupos][+ o -][permisos de acceso] archivo
#+END_EXAMPLE

- asignar *+* :: permite asignar el permiso al grupo o usuario
- asignar *-* :: permite denegar los permisos

** Ejemplos para asignaciones simbólicas

Asignar los permisos con notación simbólica.

Por ejemplo, si deseo que un archivo sea solo de escritura para el usuario:

#+BEGIN_SRC bash
chmod u+r archivo
#+END_SRC

O bien, sea de ejecución para todos los usuarios:

#+BEGIN_SRC bash
chmod a+x archivo
#+END_SRC

** Referencias de permisos de usuario.

1.- Permisos de usuario, grupo sobre archivos y directorios 
https://thelinuxalchemist.wordpress.com/2017/04/04/gestion-de-permisos-en-linux/

2.- Comando chown http://rm-rf.es/chown/

3.- Usuarios y grupos 
http://www.ite.educacion.es/formacion/materiales/85/cd/linux/m1/administracin_de_usuarios_y_grupos.html


* Permisos de ejecución en scripts
** Reconocer un interprete de un lenguaje

Hasta el momento hemos estudiado ~bash~, ~sed~ y ~awk~ como lenguajes
interpretados, también un poco ~python~.

Es decir, cada *script* que hemos escrito lo lee una /maquina/ que interpreta la
serie de comandos y realiza la serie de acciones programadas. Eso es *programar*
y *ejecutar un programa*, pero también es *intepretar computacionalmente un
texto*. De ahí el nombre.

** Ubicación del interprete

El *interprete* es un archivo binario ejecutable que se ubica en algún
directorio del sistema.

Por lo general

- bash :: /bin/bash
- sed :: /bin/sed
- awk  :: /bin/awk
- python :: /bin/python3

De otra manera, se ubica en el directorio de compilación. Por ejemplo, cuando
deseamos cambiar el *interprete* que tiene algún usuario por defecto, debemos
dar esa ruta.

** El ~shebang~ al inicio de los scripts

Para ahorrarnos o ahorrar a otras personas identificar un *lenguaje* en que está
escrito un programa, asegurando la correcta ejecución. Podemos definir el
intérprete al inicio de cada script, como corresponda. Añadimos *"#! "* a la
ruta del binario.

- bash :: #! /bin/bash
- sed :: #! /bin/sed
- awk  :: #! /bin/awk -f
- python :: #! /bin/python3

Luego, pasamos  a otorgar permisos de ejecución.

** Otorgar permisos de ejecución

A modo de ejercicio, tenemos dos scripts realizados durante el curso:

- pedirtexto.sh
- varios_textos/selector.awk

A ambos les falta su *shebang*, ponerle la cadena correspondiente.

Luego, otorgamos permisos de ejecución, para que todos los usuarios puedan ejecutarlo:

#+BEGIN_SRC bash
chmod a+x [script]
#+END_SRC

Para que solo el usuario creador lo haga:

#+BEGIN_SRC bash
chmod u+x [script]
#+END_SRC

* Crear un script con opciones o entradas no posicionales

** Pasar de argumentos posicionales a argumentos opcionales

Una manera de crear *software* flexible es permitiendo que algunas opciones o
información que deba ser entregada sea opcional. 

El modo posicional pedirá siempre que la información esté en la posición
adecuada. A veces no es necesario, pero necesitaremos indagar un poco más para
que sea posible.

** El comando ~getopts~, argumentos con banderas

Hemos visto que algunos comandos se accionan con opciones que
contienen el símbolo *menos* o *-* precedido por alguna letra.

Esta letra específica o bandera (*flag*), activará o desactivará
alguna acción del programa.

También es posible juntar varias letras a continuación del *-* y el sistema 
las leerá por serparado.

La funcionalidad que permite realizar el reconocimiento de estas *banderas* es
*getopts*, e investigaremos como se utiliza.

** Síntaxis de ~getopts~

El comando *getopts* (con s como último carácter) considera que nosotros
declaramos una serie de letras que acepta como argumento.

file:./img/getopts_arg_pos.png

Cada parámetro de entrada o argumento, entra en una iteración en la que se le
asigna el valor *OPTIND* por defecto. Contiene el número de opciones de la
entrada. Es necesario que esta variable vuelva a su valor original una vez
utilizada en la lectura de las opciones, con.

#+BEGIN_SRC bash
shift $(($OPTIND-1))
#+END_SRC

** Interpretación de ~getopts~

Entre las condiciones expresadas por el manual de este comando, se utiliza
el carácter *":"* (dos puntos, en /inglés/ colon) para declarar que esa opción
o bandera específica trae un argumento adicional que debe ir separado por
espacio. Este *:* se debe colocar *despúes* de la letra que trae un valor adjunto.

Cuando una opción tiene argumento, este es definido como la variable *OPTARG*
de la iteración correspondiente a la opción. Es decir '-a color', la variable
*OPTARG* tomara el valor *color*.

#+BEGIN_EXAMPLE
:ryan:
#+END_EXAMPLE

Se lee así:

- control de errores al principio, poniendo ":"
- r,y,a son opciones únicas, sin valor adjunto
- n tiene un valor adjunto.

Para controlar cualquier error en el ingreso de las opciones, se debe colocar
el signo *:* /antes de todas las letras/, de esta manera es posible controlar
cuando suceda y entregar un mensaje adecuado. De tal manera que, si hay error,
direcciona la opción al caso *?)*.

** Uso de ~getopts~ con ~while~ y ~case~

Tenemos a disposición tres colores {rojo, amarillo, azul} y debemos
entregar un número que será la posición en una lista en la que irá el color.

Se debe guardar el valor de (número, color) en un archivo *fila_color.csv* si es que el
número no existe, si ya existe y es de distinto color retornar un mensaje que diga que esa posición ya
está tomada.
 
** Recuperar el estado del resultado anterior de un comando.

Por ejemplo, si buscamos una coincidencia sobre un texto, verificamos que un
valor es número o no.

#+BEGIN_SRC bash
numero=10
echo $numero|grep -E "^[0-9]+$"
#+END_SRC

La acción *echo $?* permite conocer si el resultado fue exitoso o negativo.

- echo $? --> 1 si no hubo un buen resultado
- echo $? --> 0 si hubo un buen resultado.

** Lectura de las opciones con bandera 

Iniciar un script 

#+BEGIN_SRC bash
csv_file=fila_color.csv
color=""
numero=0
while getopts :ryan: opt
do
   # codigo case 
   # siguiente diapo
   # poner aqui
done
shift $(($OPTIND-1))
#+END_SRC

** Revisar cada caso de las opciones.

Dentro del ~while~ colocar la siguiente estructura que revise la opción iterada.

#+BEGIN_SRC bash
case "${opt}"
in
    r) color=rojo
       ;;
    y) color=amarillo
       ;;
    a) color=azul
       ;;
    n) numero=$OPTARG
       ;;
    \?) echo "Opcion Inválida: $OPTARG" 1>&2
        ;;
    :) echo "Opción Inválida: $OPTARG requiere 
                     un argumento" 1>&2
       ;;
esac
#+END_SRC

** Verificar que sean valores válidos

Revisar que el valor ingresado junto a *n* sea un número entero, y que se haya
seleccionado color.

El uso de grep que se le da aquí es obteniendo un resultado y luego recuperando
0 o 1, según lo que resulte. El comparador *-ne* quiere decir *not equal*. Si el
resultado es un número, $? retornará "0". En caso contrario 1.

Añadir. 

#+BEGIN_SRC bash
check=$(echo "$numero" | grep -qE '^[0-9]+$'; echo $?)
if [ "$check" -ne "0" ];then
    echo "Solo números enteros por favor" 1>&2
fi
if [ "$color" =~ "" ]
then
    echo "Color tiene que tener un valor" 1>&2
fi
#+END_SRC

** Procesar la información

Se añade la información, previa revisión de que exista o no el número. Si el número
no existe, se añade al archivo.

#+BEGIN_SRC bash
if [ "$check" == "0" ];then
    # buscar valor en archivo
    existe=$(grep "^$numero;" $csv_file)
    entrada="$numero;$color"
    echo "Existe -> $existe"
    echo "A archivo-> $entrada"
    if [ -z "$existe" ]
    then
        echo $entrada>>$csv_file
        echo "Se almacena combinación $entrada 
                en archivo $csv_file"
    elif [ "$existe" == "$entrada" ]; then
        echo "Ya existe esa combinación" 1>&2
    else
        echo "Esa posición está tomada" 1>&2
    fi
fi
#+END_SRC

** Poner ~shebang~ al script ~bash~ y permisos

En el inicio

#+BEGIN_SRC bash
#! /bin/bash
#+END_SRC

Luego, se otorgan los permisos

#+BEGIN_SRC bash
chmod a+x fila_color.sh
#+END_SRC

** Ejecutar script

Ahora, de manera elegante, podemos ejecutar el script e ir verificando como se
completa el archivo.

Abrir una terminal adicional.

#+BEGIN_SRC bash
tailf -n 1 fila_color.csv
#+END_SRC

Ejecutar

#+BEGIN_SRC bash
./fila_color.sh -r -n 56
./fila_color.sh -y -n 3
#+END_SRC

