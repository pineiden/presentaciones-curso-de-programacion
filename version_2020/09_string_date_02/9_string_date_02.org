#+LATEX_COMPILER: lualatex
#+TITLE: Cadenas de texto, transformaciones y formatos II
#+AUTHOR: David Pineda Osorio
#+LATEX_HEADER: \usepackage[spanish]{babel}
#+LANGUAGE: es
#+OPTIONS: H:2 toc:1 num:f
#+OPTIONS: ^:nil
#+latex_header: \usepackage{setspace}
#+latex_header: \usepackage{fontspec}
#+latex_header: \onehalfspacing

* Temario de la clase

** ¿Qué veremos en esta clase?

- Cadenas de texto :: Veremos como trabajar y manipular cadenas de textos o
     *strings*, extraer información útil de ellas y ocupar las principales
     funcionalidades

- Transformaciones y formatos :: Estudiaremos el uso de distintas herramientas
     para manipular las cadenas de texto (*strings*).

* Registrar inicio de clases
** Una marca en la línea de comandos: con nombre de clase

Para poder realizar una búsqueda que tenga un mayor contexto y precisión, 
es posible añadir más texto a la seña que nos permita mejorar la referencia.

Recomiendo añadir, al número de la clase, la seña relacionada al nombre de la
clase, que en este caso sería *strings_date*.

#+BEGIN_SRC bash
echo "CLASE_09:strings_date_02"
#+END_SRC

Anota en tu cuaderno la fecha y la seña *CLASE_09::strings_date_02*

* Operaciones con string

** El índice o posición de cada caracter

Si tenemos el siguiente texto (que es la cadena de símbolos de los digitos)

#+BEGIN_EXAMPLE
numeros=0123456789
#+END_EXAMPLE

La posición de cada uno de los caracteres sería

|---------+---+---+---+---+---+---+---+---+---+---|
| símbolo | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |
|---------+---+---+---+---+---+---+---+---+---+---|
| índice  | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 |
|---------+---+---+---+---+---+---+---+---+---+---|

El *índice* o valor de posición de cada caracter es un valor ~entero positivo~
que parte desde el valor *0* para la *primera posición*

** Ejercicio: Hacer las tabla para los siguientes textos

Hacer la misma tabla para el texto, que además contenga una fila extra para los
valores hexadecimales para:

#+BEGIN_EXAMPLE
m1=Estamos aprendiendo bash
m2=abcdefghijklmnopqrstuvwxyz
#+END_EXAMPLE

De la siguiente manera, recuerda usar el comando ~unum~


|-----------+----+----+----+----+----|
| símbolo   |  h | o  | l  |  a |  ! |
|-----------+----+----+----+----+----|
| valor hex | 68 | 6F | 6C | 61 | 21 |
|-----------+----+----+----+----+----|
| índice    |  0 | 1  | 2  |  3 |  4 |
|-----------+----+----+----+----+----|

** Extraer una subcadena (substring) de un string de largo mayor

En ocasiones, necesitaremos extraer algún trozo específico de las cadenas de
texto, estos trozos serán también cadenas de texto. 

#+BEGIN_EXAMPLE
${variable:inicio:final}
#+END_EXAMPLE

Necesitaremos conocer la /posición inicial/ y la /cantidad de caracteres/ del trozo
que deseamos extraer.

#+NAME: subtrings
#+BEGIN_SRC bash
#!/bin/bash

string=abcABC123ABCabc

echo ${string:0}
echo ${string:0:1}
echo ${string:7}
echo ${string:7:3}
#+END_SRC

** Extracción de strings usando valores negativos en el index

Cuando por el valor final usamos *valor negativo*, el /substring/ obtenido será:
desde la posición indicada (inicial) hasta la /posición final MENOS el valor final/.

#+NAME: subtrings
#+BEGIN_SRC bash
echo ${string:7:-3}
echo ${string:7:-1}
# provoca un error porque no existe cadena negativa
echo ${string:7:-10}
#+END_SRC

En cambio, si al inicio se coloca un valor negativo, siempre el substring será
el mismo string completo.

#+BEGIN_SRC bash
echo ${string: -4}
echo ${string: -4:2}
#+END_SRC

** Pasar todo a mayúsculas.

Usando dos veces ~caret~: $"\string^\string^"$ así:

#+BEGIN_SRC bash
tigres="Un tigre, dos tigres, tres tigres"
# buscar coincidencia de 'dos'
echo "Frase en mayúsculas:"${tigres^^}
#+END_SRC

¿Qué pasa si solo se usa /solo una/ $"\string^"$? Prueba con distintos textos.

** Pasar todo a minúsculas

Rápidamente un texto puede pasar a minúsculas usando doble coma: $",,"$

#+BEGIN_SRC bash
tigres="Un tigre, dos tigres, tres tigres"
# buscar coincidencia de 'dos'
echo "Frase en minúsculas:"${tigres,,}
#+END_SRC

¿Qué pasa si solo se usa /solo una/ $","$? Prueba con distintos textos.

** Eliminar todas las coincidencias previas

Si buscamos *dos* en *tigres*. Entonces podemos mostrar todo lo que sigue
después de la coincidencia así:

#+BEGIN_SRC bash
tigres="Un tigre, dos tigres, tres tigres"
# buscar coincidencia de 'dos'
echo "Mostrar después de 'dos' "${tigres#*dos}
#+END_SRC

Quiere decir: todo lo que hay hasta dos (inclusive), se borra.

En cambio, si deseamos buscar la /última coincidencia/ deberíamos usar doble
"##"

#+BEGIN_SRC bash
# buscar coincidencia de tigre
echo "Mostrar después de primer 'tigre' "${tigres#*tigre}
echo "Mostrar después de último 'tigre' "${tigres##*tigre}
#+END_SRC


** Eliminar todas las coincidencias a posteriori

De manera análoga, el uso de /%/ (porcentaje) nos permitirá borrar todo lo que
sigue desde la coincidencia.

#+BEGIN_SRC bash
tigres="Un tigre, dos tigres, tres tigres"
# buscar coincidencia de 'dos'
echo "Mostrar antes de 'dos' "${tigres%dos*}
#+END_SRC

Quiere decir: todo lo que hay desde dos (inclusive), se borra.

En cambio, si deseamos buscar la /primera coincidencia/ deberíamos usar doble
"%%".

#+BEGIN_SRC bash
# buscar coincidencia de tigre
echo "Mostrar antes de primer 'tigre' "${tigres%%tigre*}
echo "Mostrar antes de último 'tigre' "${tigres%tigre*}
#+END_SRC

** Buscar la posición de un texto dentro de un string.

#+BEGIN_SRC bash
tigres="Un tigre, dos tigres, tres tigres"
# buscar coincidencia de 'dos'
buscar=${tigres%%dos*}
echo "Posicion de 'dos' es:"${#buscar}
#+END_SRC


** Ejercicio, formatear nombres y apellidos correctamente.

Según la regla, los nombres propios deben ir con mayúscula al principio y minúsculas el resto de la palabra.

Hacer un programa que solicite nombre y apellido, en que sin importar como se ingresó la palabra, pase la primera letra a mayúsculas y el resto a minúsculas.

Para eso, utilice la técnica de recortar ~strings~ y ~tr~ que permite
transformar textos de una forma a otra, en este caso, mayúsculas a minúsculas en
la primera letra y mayúsculas a minúsculas en el resto.

#+BEGIN_SRC bash
palabra="estoIcolomia"
primera=${palabra:0:1}
resto=${palabra:1}
primera_corregida=$(echo $primera|tr [:lower:] [:upper:])
resto_corregida=$(echo $resto|tr [:upper:] [:lower:])
echo $primera_corregida$resto_corregida 
#+END_SRC

- url ejemplos tr :: https://www.thegeekstuff.com/2012/12/linux-tr-command/
- url más ejemplos :: https://linuxhint.com/bash_tr_command/ 

** Reemplazar subcadenas (substring)

Para reemplazar debemos utilizar el siguiente comando *${variable/subcadena/reemplazo}*.
Esto reemplazará la primera aparición de la subcadena por reemplazo en la variable, si queremos
que los cambios se realicen en todas las apariciones de la subcadena debemos usar doble
slash ("//") antes de la subcadena.

#+NAME: subtrings
#+BEGIN_SRC bash
#!/bin/bash
tigres="Un tigre, dos tigres, tres tigres"
# reemplaza solo la primera coincidencia
echo ${tigres/tigre/gato}
# reeemplaza todas las coincidencias
echo ${tigres//tigre/gato}
# uso de una regex para buscar vocalesy reemplazar por *
echo ${tigres//[aeiou]/*}
#+END_SRC

** Ejercicio tigres a cualquier palabra 

Así como directamente es posible hacer cambios. Podría utilizarse el editor
*sed* para hacer cambios en el *string*. Por ejemplo *tigre* por *gato*.

#+BEGIN_SRC bash
#!/bin/bash
tigres="Un tigre, dos tigres, tres tigres"
# reemplaza solo la primera coincidencia
echo $tigres|sed 's/tigre/gato/g'
#+END_SRC

Realizar un script que transforme la frase ~tigres~ bajo varias formas:

- Pedir una palabra o frase
- Reemplazar tigre en la primera coincidencia 
- Reemplazar tigre en la última coincidencia 
- Reemplazar global

Puede usar, de manera combinada:

- sed
- uniq
- sort
- wc

* Uso de 'expr' comando para manipulación de strings.

De manera alternativa, también es posible construir una expresión y evaluarla
con el comando *expr*.

La acción *substring* que permite recortar un string es ~substr~, indicando el
*string* y las posiciones inicial y final.


#+BEGIN_SRC bash
#!/bin/bash
tigres="Un tigre, dos tigres, tres tigres"
# reemplaza solo la primera coincidencia
expr substr $tigres 2 4
#+END_SRC

O, también, otra forma de obtener el /largo de un string/.

#+BEGIN_SRC bash
expr length $tigres
#+END_SRC


* Remover un subtring de un strings

Si se desea borrar o remover una cadena que exista dentro de un texto, se puede
utilizar la siguiente acción.


#+BEGIN_SRC bash
#!/bin/bash
tigres="Un tigre, dos tigres, tres tigres"
# reemplaza solo la primera coincidencia
echo $tigres|sed 's/tigre//g'
#+END_SRC


* Tarea: Los días y los meses

El siguiente ejercicio consiste en ocupar todo lo que ya hemos visto en clases.
Se trata de crear un script que le pida al usuario ciertos parámetros para
controlar la selección.

También la idea es aprender a gestionar tablas de datos con herramientas
sencillas pero precisas, en conjunto con las *expresiones regulares*.

Dentro de una carpeta especial para esta tarea.

1.- Crear un archivo por idioma en castellano: es, ingles: en y francés:fr. Cada
uno con los días del 1 al 7, un día por línea.

También puede ser una carpeta por idioma y dentro un archivo *dia.txt* y
*mes.txt*. Puedes escoger lo que más te acomode.

También se recomienda hacer los días y meses para lenguas de pueblos indígenas:
mapudungun mp, nagualt ng, aymará ay.

'dias_es.txt', que tenga la siguiente forma:

#+BEGIN_EXAMPLE
Lunes
Martes
Jueves
Viernes
Sábado
Domingo
#+END_EXAMPLE

Hacer lo mismo para los otros dos idiomas.

2.- Crear de la misma forma anterior, los nombres de los meses.

El archivo "meses_es.txt"

#+BEGIN_EXAMPLE
Enero
Febrero
Marzo
Abril
Mayo
Junio
Julio
Agosto
Septiembre
Octubre
Noviembre
Diciembre
#+END_EXAMPLE

Para los tres idiomas también. 

3.- Pruebas con *grep*. 

Define una variable *IDIOMA* y otra *MODO* (que puede ser día o mes), y filtra
desde un comando ls según idioma seleccionado.

#+BEGIN_SRC bash
IDIOMA="es"
MODO="dia"
ls|grep "${IDIOMA}"
ls|grep "${MODO}"
#+END_SRC

4.- Pruebas con *sed*

Dado una variable *NUMERO* de valor numérico y una variable *MODO*. Mostrar la
línea que corresponda al número escogido-

#+BEGIN_SRC bash
NUMERO=5
MODO="dia"
ls|grep "${MODO}"|xargs sed -n "${NUMERO} p"
#+END_SRC

5.- Crear *script* que interactue con un humano que pida lo siguiente:

Recordar el uso de *read*.

- Idioma (por la abreviación o el nombre)

De una manera u otra debe obtenerse la abreviación. Usando solamente lo que ya
se sabe.

#+BEGIN_EXAMPLE
Castellano o Español o Spanish -> es
ES -> es
#+END_EXAMPLE

IDEA: se puede tener una lista de nombres y la abreviación, recortar los últimos
dos caracteres.

- Modo (día o mes)

Del mismo modo sería ideal soportar los nombres para día o mes en diferentes
idiomas. Además dar la opción numérica 1: día, 2: mes.

- Número.

Solicitar un número. Verificar que sea compuesto únicamente de dígitos. No se
pide usar nombres de número ya que la complejidad sería mucha para el nivel.

Dados los parámetros anteriores, mostrar el nombre del número correspondiente.
Sea día o mes, y dependiendo del idioma.

Idea: Puedes *compartir* con tus compañeros de curso los archivos con los nombres
de los días y meses.


* Referencias

** Textos de referencia

- Manipular strings :: http://tldp.org/LDP/abs/html/string-manipulation.html
- Otro manual de strings :: https://www.thegeekstuff.com/2010/07/bash-string-manipulation/

