#+TITLE: Software para el Cálculo de Mediciones en Polarización Inducida
#+AUTHOR: David Pineda Osorio
#+LATEX_HEADER: \usepackage[spanish]{babel}
#+LANGUAGE: es
#+OPTIONS: H:2 toc:t num:t
#+OPTIONS: ^:nil
#+startup: beamer
#+LaTeX_CLASS: beamer
#+LATEX_CLASS_OPTIONS: [presentation]
#+BEAMER_THEME: PaloAlto
#+COLUMNS: %45ITEM %10BEAMER_ENV(Env) %10BEAMER_ACT(Act) %4BEAMER_COL(Col)

* Temario de la clase

** Comandos para manipular directorios

Revisaremos los comandos que, desde la terminal, nos permitan crear, modificar y
eliminar directorios.

** Comandos para manipular archivos

Revisaremos los comandos que, desde la terminal, nos permitan crear, modificar y
eliminar archivos.

** La lógica del sistema POSIX

Aprenderemos las formas generales de como funciona un sistema de comandos como
~bash~ en GnuLinux, esta es una definición estandarizada llamada ~POSIX~.

** Usuario, grupos  y permisos

Crearemos archivos ejecutables que, bajo ciertas condiciones, un usuario tenga
los permisos de lectura, escritura o ejecución. Veremos en la práctica la
diferencia entre usuario y grupo.

* Listar archivos y/o directorios


El comando para listar archivos y directorios ~ls~ permite hacer una inspección
básica de los elementos que hay en una ruta (directorio) en particular.

El modo más simple es:

#+BEGIN_SRC bash
ls
#+END_SRC

Pero, si nos dedicamos un tiempo a leer el manual.

#+BEGIN_SRC bash
man ls
#+END_SRC

Nos daremos cuenta de que hay opciones particulares que nos permitirán filtrar
de mejor manera los elementos en un directorio.

Por ejemplo:

#+BEGIN_SRC bash
ls *.csv
ls *.pdf
#+END_SRC


* Navegar en el árbol de directorios

En la carpeta ejercicios, encontrar el directorio *raiz*, ocupando los
siguientes comandos:

#+BEGIN_SRC bash
cd ruta
cd ..
cd ../..
cd ~
pwd
ls
#+END_SRC

Encontrar y moverse a:

#+BEGIN_SRC bash
rama_a
rama_b
rama_b2
rama_a1
rama_a2
rama_a3
raiz
#+END_SRC


* Copiar, pegar texto en la terminal

* Conocer el tamaño de archivos y directorios

* Editar archivos con nano

* Crear y eliminar directorios y archivos

* Generalidades de la terminal bash

* Crear un script básico
