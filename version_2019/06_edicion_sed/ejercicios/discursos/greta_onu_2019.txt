url :: https://www.youtube.com/watch?v=RxVNbxfXLoY

Discurso de Greta Thunberg en ONU, sept 2019

¿Cómo se atreven?

"Mi mensaje es que los estaremos vigilando.

Todo esto está mal. Yo no debería estar aquí arriba. Debería estar de 
vuelta en la escuela, al otro lado del océano. Sin embargo, ¿ustedes 
vienen a nosotros, los jóvenes, en busca de esperanza? ¿Cómo se atreven?

Me han robado mis sueños y mi infancia con sus palabras vacías. Y sin embargo, 
soy de las afortunadas. La gente está sufriendo. La gente se está muriendo. 
Ecosistemas enteros están colapsando. Estamos en el comienzo de una extinción 
masiva. Y de lo único que pueden hablar es de dinero y cuentos de 
hadas de crecimiento económico eterno. ¿Cómo se atreven?

Por más de 30 años, la ciencia ha sido clarísima. ¿Cómo se atreven a 
seguir mirando hacia otro lado y venir aquí diciendo que están haciendo 
lo suficiente, cuando la política y las soluciones necesarias aún no 
están a la vista?

Dicen que nos "escuchan" y que entienden la urgencia. Pero no importa 
cuán triste y enojada esté, no quiero creer eso. Porque si realmente 
entendieran la situación y de todas formas no actuaran, entonces 
serían malvados. Y eso me niego a creerlo.

La idea de reducir nuestras emisiones a la mitad en 10 años solo nos da 
un 50% de posibilidades de mantenernos por debajo de los 1,5 grados y 
el riesgo de desencadenar reacciones irreversibles en cadena más 
allá del control humano.

Quizá 50% sea aceptable para ustedes. Pero esos números no incluyen 
puntos de inflexión, la mayoría de los círculos de retroalimentación, 
calentamiento adicional oculto por la polución tóxica del aire o 
aspectos de equidad y justicia climática. También se basan en que 
mi generación absorba cientos de miles de millones de toneladas de su 
CO2 del aire con tecnologías que apenas existen.

Así que un riesgo del 50% simplemente no es aceptable para nosotros, 
que tenemos que vivir con las consecuencias.

Para tener un 67% de posibilidades de mantenernos por debajo de un 
aumento de la temperatura global de 1,5 grados, las mejores probabilidades 
dadas por el IPCC (el Panel Intergubernamental de Cambio Climático), 
el mundo tenía 420 gigatoneladas de CO2 para emitir el 1º de enero de 2018.

Hoy esa cifra ya se ha reducido a menos de 350 gigatoneladas. ¿Cómo se 
atreven a fingir que esto se pueda resolver actuando como de costumbre 
y con algunas soluciones técnicas?

Con los niveles de emisiones actuales, ese presupuesto restante de CO2 
desaparecerá por completo en menos de 8 años y medio.

Hoy no se presentarán soluciones o planes en consonancia con estas cifras. 
Porque estos números son demasiado incómodos. Y todavía no son suficientemente 
maduros como para decir las cosas como son.

Nos están fallando.Pero los jóvenes están empezando a entender su traición. 
Los ojos de todas las generaciones futuras están sobre ustedes. 
Y si eligen fallarnos, nunca los perdonaremos.

No dejaremos que sigan con esto. Justo aquí, ahora es donde trazamos la 
línea. El mundo se está despertando. Y se viene el cambio, les guste o no.

Gracias".
