#+TITLE: Primeros comandos de Bash en GnuLinux, parte I
#+AUTHOR: David Pineda Osorio
#+LATEX_HEADER: \usepackage[spanish]{babel}
#+LANGUAGE: es
#+OPTIONS: H:2 toc:t num:t
#+OPTIONS: ^:nil
#+startup: beamer
#+LaTeX_CLASS: beamer
#+LATEX_CLASS_OPTIONS: [presentation]
#+BEAMER_THEME: PaloAlto
#+COLUMNS: %45ITEM %10BEAMER_ENV(Env) %10BEAMER_ACT(Act) %4BEAMER_COL(Col)
#+latex_class_options: [10pt]
#+latex_header: \usepackage{setspace}
#+latex_header: \onehalfspacing

* Temario de la clase

 - Comandos para manipular directorios ::
Revisaremos los comandos que, desde la terminal, nos permitan crear, modificar y
eliminar directorios.
 - Comandos para manipular archivos ::
Revisaremos los comandos que, desde la terminal, nos permitan crear, modificar y
eliminar archivos.

* Registrar Inicio de Clases
** Una marca en la línea de comandos

Todas las clases deberemos registrar el inicio de clases para resguardar y
tener almacenadas nuestras acciones.

Usaremos el comando ~echo~ para dejar una marca

#+BEGIN_SRC bash
echo "CLASE_01"
#+END_SRC

Anota en tu cuaderno la fecha y la seña *CLASE_01*

* Descargar o Clonar las presentaciones

** Descargar el repositorio de las presentaciones

En el sitio web *www.gitlab.com*, dirigirse a:

- url :: https://gitlab.com/pineiden/presentaciones-curso-de-programacion

Y descargar el repositorio desde el botón de la "/nube/".

[[file:./img/gitlab.png]]

** Clonar el repositorio de las presentaciones


Tomar la dirección del repositorio y clonarlo (debes tener git instalado)

#+BEGIN_SRC bash
git clone https://gitlab.com/pineiden/presentaciones-curso-de-programacion
#+END_SRC

* Copiar ejercicios de clases a carpeta personal

** Algo a realizar en cada clase

Cada nueva clase deberás actualizar el proyecto en tu carpeta local.

El *proyecto* es la carpeta en que está localizado el directorio de las presentaciones del curso.

#+BEGIN_SRC bash
RUTA_PRESENTACIONES=/home/david/presentaciones_curso_programacion
cd $RUTA_PRESENTACIONES
git pull
#+END_SRC

** Crear carpeta de trabajo y de clase específica

Luego, cada clase tendrá una carpeta de ejercicios *cuyo interior* debe ser
copiado en tu carpeta propia de trabajo. 

Si tu carpeta de trabajo es */home/david/Documentos/curso_programacion*,
entonces crea una carpeta con el nombre o código de la clase y copia dentro el
contenido de la carpeta de *ejercicios* (lo que está adentro).

#+BEGIN_SRC bash
cd /home/david/Documentos/curso_programacion
mkdir clase_numero_actual
#+END_SRC

** Copiar y pegar ejercicios

Esta acción debes realizarla siempre *ANTES* de clases, de manera que exista
fluidez en el trabajo.

#+BEGIN_SRC bash
cd PresentacionesCurso/clase_n
origen=ejercicios
destino=/home/david/Documentos/curso_programacion/clase_numero_actual
cp $origen/* $destino/*
#+END_SRC

* Listar archivos y/o directorios
** Iniciar con ls

El comando para listar archivos y directorios ~ls~ permite hacer una inspección
básica de los elementos que hay en una ruta (directorio) en particular.

El modo más simple es:

#+BEGIN_SRC bash
ls
#+END_SRC

Pero, si nos dedicamos un tiempo a leer el manual.

#+BEGIN_SRC bash
man ls
#+END_SRC

** Hacer cosas interesantes con ls

Nos daremos cuenta de que hay opciones particulares que nos permitirán filtrar
de mejor manera los elementos en un directorio.

Por ejemplo, la segunda parte corresponde a un filtro en que especificamos
solamente un tipo de extensión:

#+BEGIN_SRC bash
ls *.csv
ls *.pdf
#+END_SRC

** Usar ls con opciones

O bien, para listar con algunas opciones (según el manual):

#+BEGIN_SRC bash
ls -F 
ls -a
#+END_SRC

La segunda parte del comando corresponde a una de las opciones disponibles para
el comando.

* Navegar en el árbol de directorios

** Usar los comandos para tener información de directorios

En la carpeta ejercicios, encontrar el directorio *raiz*, ocupando los
siguientes comandos:

#+BEGIN_SRC bash
cd ruta
cd ..
cd ../..
cd ~
pwd
ls
#+END_SRC

** Moverse usando los comandos a

Encontrar y moverse a:

#+BEGIN_SRC bash
rama_a
rama_b
rama_b2
rama_a1
rama_a2
rama_a3
raiz
#+END_SRC

Escribe en tu cuaderno que hiciste para moverte a cada uno de ellos de la manera
siguiente:

- pwd (¿Qué te entrega?)
- objetivo -> comando

* Acciones sobre el texto de la terminal

** Seleccionar, copiar y pegar texto en la terminal

Considera los siguientes atajos de teclado:

- ^S :: tecla shift
- ^M :: tecla alt
- ^C :: tecla control
- ^SPC :: tecla espacio

- Seleccionar :: con el mouse o bien con shift y flecha
- Copiar :: ^C+^S+c
- Pegar :: ^C+^S+v

También puedes pegar texto al seleccionarlo y apretar el botón del medio del mouse.

* Conocer el tamaño de archivos y directorios

** El espacio utilizado por discos y directorios

A veces, será necesario que tengas un criterio respecto al uso del espacio en
disco duro o el tamaño de un directorio.

- df :: usa el comando df para conocer el uso general de cada dispositivo de memoria
- du :: te será útil para conocer el tamaño de un directorio
- baobab :: es una interfaz gráfica que te muestra el uso del disco

*¿Qué es la opción -h?*

* Manipulación de archivos

** Crear

La creación de un archivo se puede realizar con un comando ~touch~, una vez
definida la ruta.

Crea un *archivo de texto* que puedes completar, teniendo el directorio base:

#+BEGIN_SRC bash
BASE_DIR=/ruta/base
touch $BASE_DIR/nuevo_archivo
#+END_SRC

Como ejercicio, crear los archivos, en /raiz/

- hojita_1
- hojita_2
- rama_1/hojita_3

** Editar

Para editar archivos existe varias herramientas y comandos que, dependiendo lo
que necesitemos, los tendremos que utilizar. Entre estos están 

- > :: toma la salida estándar y crea un nuevo archivo
- >> :: toma la salida estándar y /agrega/ las nuevas líneas al archivo (lo crea
        si no existe)
- sed :: un comando para editar o modificar archivos
- nano :: programa de edicición de textos desde la terminal.

** Uso de nano

Utilizando *nano*, las acciones base son:

- ^C+o :: para guardar
- ^C+x :: para salir

Como ejercicio, escribe en los archivos anteriores las características de cada
*hojita*

- color
- tamaño
- fecha de nacimiento

** Borrar

Para borrar archivos solo basta usar el comando *rm*

#+BEGIN_SRC bash
rm archivo_a_borrar
#+END_SRC

Como ejercicio, borrar *hojita_2*

* Resguardar los comandos realizados

Cada sesión realizaremos una serie de comandos, estos se almacenan en memoria.

En cada clase haz lo siguiente:

#+BEGIN_SRC bash
history
echo "CLASE #NUMERO"
#+END_SRC

Al finalizar guarda los comandos realizados en un archivo. (es un comando que
veremos a futuro, pero úsalo). Crea antes la carpeta *historia*, en dónde te acomode.

#+BEGIN_SRC bash
history | grep -A 1000 "CLASE #NUMERO" > 
historia/clase_numero.sh
#+END_SRC

