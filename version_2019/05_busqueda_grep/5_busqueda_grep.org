#+TITLE: Textos en Bash y búsqueda con grep
#+AUTHOR: David Pineda Osorio
#+LATEX_HEADER: \usepackage[spanish]{babel}
#+LANGUAGE: es
#+OPTIONS: H:2 toc:t num:t
#+OPTIONS: ^:nil
#+startup: beamer
#+LaTeX_CLASS: beamer
#+LATEX_CLASS_OPTIONS: [presentation]
#+BEAMER_THEME: PaloAlto
#+COLUMNS: %45ITEM %10BEAMER_ENV(Env) %10BEAMER_ACT(Act) %4BEAMER_COL(Col)
#+latex_class_options: [10pt]
#+latex_header: \usepackage{setspace}
#+latex_header: \onehalfspacing

* Temario de la clase
** ¿Qué veremos en esta clase?

- Aprender a sobre los textos y sus transformaciones informáticas ::
Estudiaremos el texto de los archivo, aprender a detectar y definir ciertos patrones, 

- Aprender buscar información en archivos basados en texto ::


* Registrar Inicio de Clases
** Una marca en la línea de comandos

Todas las clases deberemos registrar el inicio de clases para resguardar y
tener almacenadas nuestras acciones.

Usaremos el comando ~echo~ para dejar una marca

#+BEGIN_SRC bash
echo "CLASE_05"
#+END_SRC

Anota en tu cuaderno la fecha y la seña *CLASE_05*

** Otro estilo de marca: con nombre de clase

Para poder realizar una búsqueda que tenga un mayor contexto y precisión, 
es posible añadir más texto a la seña que nos permita mejorar la referencia.

Recomiendo añadir, al número de la clase, la seña relacionada al nombre de la
clase, que en este caso sería *busqueda_grep*.

#+BEGIN_SRC bash
echo "CLASE_05::busqueda_grep"
#+END_SRC

Anota en tu cuaderno la fecha y la seña *CLASE_05::busqueda_grep*

* ¿Cómo buscamos en un archivo de texto?

** Buscar en "10 consejos para escribir un cuento"

Leer el archivo que está en /ejercicios/10_consejos_cuento_cortazar.txt/ y
responder las siguientes preguntas

- ¿Qué dice el consejo n° 8?
- ¿En cuáles consejos se habla de narrador?
- ¿Dónde se habla comparando con la novela?
- ¿Qué cadena de símbolos se ocupa para demarcar un título?

** La experiencia de búsqueda

La lectura de un texto consiste en que sabemos, en el caso del español
que es *secuencial* desde *arriba  a abajo* y se lee de *izquieda a derecha*.

Los números y caracteres también son ~secuencias de texto~.

Todo símbolo en un archivo *es* una ~secuencia de texto~.

La capacidad humana de lectura es *lenta* comparada con lo que hace
un computador.

** Ejercicio con git

Añadir el texto *10 consejos para escribir un cuento* al proyecto *Nuestra
selección literaria*

- hacer el commit
- hacer el push a la plataforma ~gitlab~
- ¿le faltaría alguna información adicional?
- añadir la información adicional
- hacer commit
- hacer el push 

* ¿Cómo se busca en un editor?

** En nano

En ~nano~ puedes buscar texto dentro del archivo que estás editando utilizando
la combinación de teclas *Ctrl+w*, que activa un campo de búsqueda, al apretar
*enter* te lleva a la primera coindidencia.

[[file:./img/buscar_nano.png]]

Si nuevamente apretas la combinación *Ctrl+w* y no ingresas nueva búsqueda, te
llevará a la siguiente coindcidencia.

[[file:./img/buscar_nano_campo.png]]

** En geany

El editor ~geany~ es recomendado como un editor para iniciar en la programación
por ser muy versátil y sencillo.

Se puede activar la búsqueda mediante la combinación *Ctrl+f*, activará un
formulario de búsqueda con opciones.

[[file:./img/geany_buscar.png]]

* ¿Qué es grep?
** Un comando para buscarlo todo: grep
El comando ~grep~ es uno de los que te serán de mucha utilidad para
poder buscar y filtrar información en el sistema de archivos.

Permite realizar búsquedas sobre /archivos/ en particular, sobre /streams/ de
datos, con distintas opciones ajustadas a lo que necesitemos.

Nos entrega la posibilidad de usar *patrones* o *expresiones regulares*, que
definen un *conjunto de cadenas de texto*.

Se utiliza de la siguiente manera

#+BEGIN_EXAMPLE
grep [OPCIONES] [PATRON_TEXTO] ARCHIVO(s) 
#+END_EXAMPLE

** Buscar o filtrar desde un stream

O bien, tomando un ~stream~

#+BEGIN_EXAMPLE
comando_cualquiera | grep [OPCIONES] [PATRON_TEXTO] 
#+END_EXAMPLE

*** Ejercicio

- ¿Cómo realizaría el mismo ejercicio inicial utilizando grep?
- ¿Cuál sería el patrón de texto y el archivo en cada caso?

** El manual de grep

Activar el manual de ~grep~ nos permitirá acceder a la explicación
fundamental de cómo opera el comando y cuales serían las posibles opciones
que tenemos disponibles.

#+BEGIN_SRC bash
man grep
#+END_SRC

En general, el resultado de ejecutar este comando resultará en una /lista/ de
coincidencias o, en inglés, *match*. 

Las entrega a través de la *salida estándar* como *stream*.

*** Ejercicio

- ¿Qué dice el manual de las expresiones regulares?
- ¿Qué quiere decir la opcion *-v*?
- ¿Cómo obtener el número de línea?

* Uso de grep en un archivo

** Buscando comidas en la /Epopeya de las comidas y bebidas chilenas/

En el archivo 'ejercicios/epopeya_comidas.txt', que contiene algunos extractos
del libro *Epopeya de las comidas y bebidas chilenas* del gran poeta chileno
*Pablo de Rocka* nos interesa buscar los siguientes ingredientes para iniciar
nuestra investigación culinaria.

- chanfaina
- chunchul trenzado
- caldo de ganso

** Ejercicios con el texto epopeya_comidas.txt

- ¿Qué se hacía con esos ingredientes?
- ¿Se preparan aún esas comidas? ¿Dónde?
- leer el libro, descárgalo de [[http://www.memoriachilena.gob.cl/602/w3-article-93714.html][Memoria Chilena]]
- ¿Existe alguno de los preparados con recetas vegetariana o vegana?

* Uso de grep tomando un stream
** Seleccionar archivos en base a un patrón de texto.

Ya conocemos el efecto que nos entrega el comando ~ls~, realizaremos una
combinación con una selección filtrada con ~grep~ y algún /patrón/ de texto.

Vamos a 'ejercicios/listar', 

- ¿Qué archivos hay?

Encadenar con ~grep~ y registrar los resultados en tu cuaderno, con los
siguientes /patrones de texto/

- cuent
- cuento
- cuentito
- *.txt

** Con una búsqueda a través del árbol de directorios

Si te has fijado, existe una carpeta *otros*, que también contiene algunos
textos.

#+BEGIN_SRC bash
find -iname "*"
#+END_SRC

Luego, realiza la misma búsqueda con los patrones anteriores, ¿qué resulta?

*** Ejercicio

Investigar como buscar coincidencias dentro de los archivos, si logramos obtener
ya la ruta a el archivo.

* Buscar sobre la jerarquía de archivos

** Transformar un stream en argumento del siguiente comando

Un ~stream~ entrega normalmente texto a filtrar que procesa el comando
siguiente, sin embargo es posible transformar ese texto a que pase a ser el
argumento principal de entrada del siguiente comando.

Para eso se ocupa la acción de ~xargs~ que transforma lo que entra desde la
tubería a un *input* principal.

El uso general es (según *man xargs*)

#+BEGIN_EXAMPLE
comando_cualquiera|xargs otro_comando
#+END_EXAMPLE
** Buscar dentro del contenido de los archivos seleccionados

Para el caso de ~grep~ es útil la siguiente estructura, que permite filtrar
ciertos archivos que coincidan con un patrón de texto y buscar luego dentro de
cada uno de ellos.

Por ejemplo para buscar la palabra *emperador* dentro de todos los archivos que
sean *cuento*.

#+BEGIN_SRC bash
find -iname "*"|grep cuento|xargs grep emperador
#+END_SRC

* Mostrar las líneas antes y despues de coincidencia

** Mostrar líneas relacionadas a coincidencia

Con ~grep~ es posible controlar el *contexto* de visualización de las
coincidencias que ocurran.

Esto es útil cuando necesitamos leer o extraer información relacionada a la
búsqueda.

Para eso tenemos un conjunto de opciones, con mayúsculas.

- B NUM :: del inglés *before*, permite mostrar *NUM* líneas antes de coincidir.  
- A NUM :: del inglés *after*, permite mostrar *NUM* líneas después de coincidir.  
- C NUM :: del inglés *context*, muestra *NUM* líneas en torno a coincidencia.

** Ejercicio con Nuestra Selección Literaria

Realizar lo siguiente, siendo:

- N :: =3
- M :: =4

Escoger una búsqueda extensa en todos los archivos de tu proyecto. Una /palabra/
o /expresión/. Y mostrar:

- N líneas antes de coincidir
- M líneas después de coincidir
- Mostrar N+M líneas en torno a coincidencia

* Expresiones regulares

** Una forma de hacer conjuntos de palabras

Puede ocurrir que necesitemos encontrar coincidencias sobre un conjunto de
palabras o cadenas de texto y luego realizar acciones sobre ellas.

Para eso se debe definir, de alguna manera, un conjunto de palabras que las
representen.

Por ejemplo, si deseamos:

- BUSCAR :: {papá, mamá}
- ACCIÓN :: pasar a mayúsculas


** Definir un patrón general 

Podemos definir un patrón de texto que *represente el conjunto*

- PATRÓN :: "[pm]a[pm]á"

De esta manera, si buscamos con *PATRÓN*, utilizando ~grep~, deberíamos
escribir. realizar el ejercicios sobre el archivo *primer_regex.txt*

#+BEGIN_SRC bash
grep "[pm]a[pm]á" primer_regex.txt
#+END_SRC

*** Ejercicio

- ¿Cómo pasamos a mayúsculas las coindidencias?
- Investigar el uso de ~sed~

* El conjunto de herramientas

** La trifuerza POSIX

Dentro de todo el conjunto de herramientas disponibles del sistema
*POSIX*, lo podemos entender como la *trifuerza* esencial con
{~grep~, ~sed~, ~awk~} y el uso de las ~expresiones regulares~.

#+NAME:   fig: trifuerza_posix
#+ATTR_HTML: width="150px"
#+ATTR_ORG: :width 150
#+attr_latex: :width 150px
[[file:./img/trifuerza.png]]

* Material de referencia

** Lecturas a estudiar
 
- Buscador Pirata Library Gen Ru :: http://gen.lib.rus.ec
- Libro :: Grep, pocket reference (buscar en Libgenru)
- Ejemplos :: https://www.comoinstalarlinux.com/linux-grep/
- Wikipedia grep :: https://es.wikipedia.org/wiki/Grep

** Lecturas sobre regex 


- Libro de regex :: Regular Expressions (buscar en https://sites.google.com/site/themetalibrary/library-genesis]]
- Gui Regex :: https://www.adictosaltrabajo.com/tutoriales/regexsam/
- Regex Cheet :: https://www.cheatography.com/davechild/cheat-sheets/regular-expressions/pdf/
- Wikipedia regex :: https://es.wikipedia.org/wiki/Expresi%C3%B3n_regular
- Inicio rápido :: http://www.rexegg.com/regex-quickstart.html
