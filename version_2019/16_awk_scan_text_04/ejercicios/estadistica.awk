function calc_promedio(valores_temp){
#valores_temp es una lista de temperaturas
sumando=0
for (posicion in valores_temp){
valor=valores_temp[posicion];
sumando+=valor;
}
promedio=sumando/length(valores_temp);
return promedio
}

function calc_std(valores, promedio){
sumando=0
for (posicion in valores){
valor=valores[posicion];
cuadrado=(valor-promedio)^2;
sumando+=cuadrado;
}
desv_std=sqrt(sumando/length(valores));
return desv_std
}
