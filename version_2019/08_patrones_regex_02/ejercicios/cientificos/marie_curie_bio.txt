
Comentarios
Biografía de Marie Curie

Maria Salomea Skłodowska-Curie, también conocida como Marie Curie, fue una 
científica. Nació el 7 de noviembre de 1867 en Varsovia, Polonia. Marie Curie 
pasó a la historia por haber descubierto junto a su esposo Pierre Curie, la 
radiactividad. Ella le abrió las puertas a la lucha contra varias enfermedades 
crueles.

Maria Salomea Skłodowska, luego conocida como Marie Curie, al acoger el 
apellido de su esposo Pierre Curie. Era la quinta hija del profesor de física 
y matemáticas, Władysław Skłodowski, y de la maestra Bronisława Boguska. 
Marie fue una gran estudiante que se sentía atraída por la física y las 
matemáticas. Ese gustó duró hasta su juventud, entonces Marie quería 
especializarse en Ciencias Físicas, pero la Polonia de ese tiempo, como era 
sometida por la Rusia zarista, les negaba a las mujeres tener un estudio 
superior. Marie decidió que tenía que salir de su país natal para poder 
estudiar Ciencias Físicas.

En 1890, su hermana Bronisława se casó y meses después invitó a Marie para 
que se fuese a vivir con ellos en París, pero ella se negó porque no tenía 
el dinero suficiente para pagar su matrícula universitaria. Recibió ayuda 
económica de su padre y siguió trabajando hasta reunir el dinero suficiente 
para poder viajar.

A finales de 1891, Maria viajó a Francia, lo primero que hizo fue inscribirse 
en la Soborna y para poder integrarse cambia su nombre, a partir de ahí se 
llamaría Marie. En ese tiempo Marie tenía 24 años y su única meta era 
iniciar sus estudios universitarios. Marie Curie vivió un tiempo con su 
hermana y su cuñado, hasta que consiguió alquilar una habitación en el 
Barrio Latino de París. Para Marie lo único importante era su carrera 
universitaria. Su insuficiencia económica, la anemia, el hambre y el frío no 
fueron obstáculos para llevar acabo ello. Marie consiguió su licenciatura en 
Física en 1893 y en Matemáticas un año después.

En 1894 la vida de Marie tomó un cambio, pues en ese año Marie conocen la 
universidad a Pierre Curie, científico francés, que trabajaba de profesor en 
la Soborna. En ese año trabajaron juntos en el laboratorio de la facultad. La 
pasión que ambos tenía pos las ciencias, poco a poco se fue volviendo algo 
más íntimo. Marie y Pierre se casaron en 1895. Su luna de miel fue recorre 
toda Francia en sus bicicletas.

Al volver a casa, el matrimonio se enfocó en sus tareas científicas. 
Convirtieron su casa en un laboratorio algo improvisado, y en el invirtieron 
todo su tiempo libre en ir avanzando sus investigaciones.

En 1897 nació su primera hija Irène. Las obligaciones como madre y esposa no 
afectaron en sus investigaciones. En ese mismo año Marie terminó sus estudios 
universitarios y fue becada. Publicó su primer trabajo científico, una 
monografía sobre la imantación del acero templado. Marie estaba buscando un 
tema interesante para su tesis doctoral. Ella se encontró con el 
descubrimiento casual que había hecho Antoine Henri Bequerel en febrero de 
1896: La radiactividad natural. A Marie le llamó mucho la atención y quedó 
fascinada, entonces a partir de ahí Los Curie empezaron a investigar el 
fenómeno y a formular las bases que consiguieran aclarar este descubrimiento.

Marie le contagió su interés por el misterio de esas irradiaciones a su 
marido Pierre. El matrimonio Curie inició sus investigaciones y descubrieron 
que no sólo el uranio emitía los rayos descubiertos por Becquerel. También 
repararon en que la pechblenda, un mineral que es extraído del uranio, era 
mucho más radioactivo que este.

Debían encontrar los otros elementos radioactivos que contenía la pechblenda 
y comprender el porqué de sus radiaciones. Su trabajo radicó en procesar y 
separar esos elementos. La pareja de los Curie trabajaba en buena armonía, 
Pierre se dedicaba a observar las propiedades de ñas radiaciones y Marie a 
purificar los elementos que los producían.

Los Curie son, en buena parte, responsables de la transformación de la 
investigación científica moderna. Ellos demostraron que la radiación no se 
producía como resultado de una reacción química, sino que hacía parte de 
una propiedad del mismo elemento, de su átomo. Ellos dieron paso al desarrollo 
del estudio de la energía nuclear, clave en el acontecer del siglo XX.

En 1898 Descubrieron el gas radón y la radiactividad del Torio. Los Curie 
anunciaron en julio de este año el descubrimiento de un nuevo elemento 
también radioactivo, al que Marie nombró Polonio en honor a su tierra natal. 
A finales de ese año, los Curie presentan otro nuevo elemento químico, el 
Radio, del que afirmaron que ese elemento emitía una reacción que era 
muchísimo mayor a la del Uranio. Estos descubrimientos les dieron 
reconocimiento mundial a los Curie. El matrimonio se negó a patentar su 
descubrimiento para que la Ciencia pudiese profundizar más en ello.

Marie Curie Infografía

 

En 1903 Ganó el premio Nobel de física junto a su marido Pierre y Antoine 
Henri Becquerel. En 1904 nació su segunda hija, Eve. Para ese tiempo Marie 
Curie estaba agotada físicamente.

En 1906 Muere su esposo, Pierre Curie. Tras la muerte de su esposo, Marie Curie 
obtiene en 1910 una cátedra de física, que su marido dejó en la Soborna, 
volviéndose así la primera mujer que dictaba clase en la famosa universidad. 
Unos años antes la Soborna y el instituto Pasteur de París habían creado el 
instituto del Radio, cuyo fin era investigar más sobre este tema y las 
aplicaciones médicas de la radioactividad. Marie Curie fue la directora de esa 
institución.

En 1911 Marie Curie recibe su segundo Nobel, pero esta vez es el Nobel de 
Química. Antes de ella, nadie había ganado nunca dos premios Nobel.

Marie Curie falleció a causa de la leucemia a sus 67 años, el 4 de julio de 
1934, en París.
